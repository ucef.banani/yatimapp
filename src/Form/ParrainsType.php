<?php

namespace App\Form;

use App\Entity\Parrains;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParrainsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('tele')
            ->add('email')
            ->add('mediateur')
            ->add('prix')
            ->add('dateDebut')
            ->add('dateFin')
            ->add('date')
            ->add('problemePause')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Parrains::class,
        ]);
    }
}
