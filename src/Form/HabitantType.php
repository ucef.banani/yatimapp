<?php

namespace App\Form;

use App\Entity\Habitant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HabitantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('relationOrpheline')
            ->add('metier')
            ->add('etablissement')
            ->add('sallaire')
            ->add('charge')
            ->add('loisir')
            ->add('sante')
            ->add('remarque')
            ->add('typeMaladie')
            ->add('chargeTrv')
            ->add('maladieGrave')
            ->add('endicape')
            ->add('suivie')
            ->add('chargeFixe')
            ->add('remarque2')
            ->add('casUrgent')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Habitant::class,
        ]);
    }
}
