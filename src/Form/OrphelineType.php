<?php

namespace App\Form;

use App\Entity\Orpheline;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrphelineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('sexe')
            ->add('dateNaissance')
            ->add('scolaire')
            ->add('branche')
            ->add('transScolaire')
            ->add('transCharge')
            ->add('autreCharge')
            ->add('faiblesseScolaire')
            ->add('loisir')
            ->add('quoran')
            ->add('pray')
            ->add('tele')
            ->add('typeVetement')
            ->add('typeSnate')
            ->add('remarque')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Orpheline::class,
        ]);
    }
}
