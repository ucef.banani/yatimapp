<?php

namespace App\Entity;

use App\Repository\OrphelineRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrphelineRepository::class)
 */
class Orpheline
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $sexe;

    /**
     * @ORM\Column(type="date",  nullable=true)
     */
    private $dateNaissance;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $scolaire;

     /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $etablissement;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $branche;
    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $transScolaire;
    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $transCharge;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $autreCharge;
    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $faiblesseScolaire;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $pasScolaire;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $loisir;
    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $quoran;
    /**
     * @ORM\Column(type="string", length=255 ,  nullable=true)
     */
    private $pray;
    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $tele;


    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $codeMassar;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $passMassar;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $typeVetement;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $sizeVetement;


    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $chaussure;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $chausUrgent;


    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $typeSnate;
    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $remarque;


    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $typeMaladie;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $chargeTrv;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $maladieGrave;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $endicape;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $typeEndicape;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $suivie;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $chargeFixe;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $remarque2;
    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $casUrgent;

    

    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="orphelines")
     */
    private $dossier;


    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="orpheline")
     */
    private $pictures;

    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="orphelineMedical")
     */
    private $medicalPictures;

    /**
     * @ORM\ManyToOne(targetEntity="Parrains", inversedBy="orphelines")
     */
    private $parrain;


    public function __construct()
    {

        $this->medicalPictures = new ArrayCollection();
        $this->pictures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getScolaire(): ?string
    {
        return $this->scolaire;
    }

    public function setScolaire(string $scolaire): self
    {
        $this->scolaire = $scolaire;

        return $this;
    }

    public function getEtablissement(): ?string
    {
        return $this->etablissement;
    }

    public function setEtablissement(string $etablissement): self
    {
        $this->etablissement = $etablissement;

        return $this;
    }

    public function getBranche(): ?string
    {
        return $this->branche;
    }

    public function setBranche(string $branche): self
    {
        $this->branche = $branche;

        return $this;
    }

    public function getTransScolaire(): ?string
    {
        return $this->transScolaire;
    }

    public function setTransScolaire(string $transScolaire): self
    {
        $this->transScolaire = $transScolaire;

        return $this;
    }

    public function getTransCharge(): ?string
    {
        return $this->transCharge;
    }

    public function setTransCharge(string $transCharge): self
    {
        $this->transCharge = $transCharge;

        return $this;
    }

    public function getAutreCharge(): ?string
    {
        return $this->autreCharge;
    }

    public function setAutreCharge(string $autreCharge): self
    {
        $this->autreCharge = $autreCharge;

        return $this;
    }

    public function getFaiblesseScolaire(): ?string
    {
        return $this->faiblesseScolaire;
    }

    public function setFaiblesseScolaire(string $faiblesseScolaire): self
    {
        $this->faiblesseScolaire = $faiblesseScolaire;

        return $this;
    }

    public function getPasScolaire(): ?string
    {
        return $this->pasScolaire;
    }

    public function setPasScolaire(string $pasScolaire): self
    {
        $this->pasScolaire = $pasScolaire;

        return $this;
    }

    public function getLoisir(): ?string
    {
        return $this->loisir;
    }

    public function setLoisir(string $loisir): self
    {
        $this->loisir = $loisir;

        return $this;
    }

    public function getQuoran(): ?string
    {
        return $this->quoran;
    }

    public function setQuoran(string $quoran): self
    {
        $this->quoran = $quoran;

        return $this;
    }

    public function getPray(): ?string
    {
        return $this->pray;
    }

    public function setPray(string $pray): self
    {
        $this->pray = $pray;

        return $this;
    }

    public function getTele(): ?string
    {
        return $this->tele;
    }

    public function setTele(string $tele): self
    {
        $this->tele = $tele;

        return $this;
    }

    public function getTypeVetement(): ?string
    {
        return $this->typeVetement;
    }

    public function setTypeVetement(string $typeVetement): self
    {
        $this->typeVetement = $typeVetement;

        return $this;
    }

    public function getTypeSnate(): ?string
    {
        return $this->typeSnate;
    }

    public function setTypeSnate(string $typeSnate): self
    {
        $this->typeSnate = $typeSnate;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getTypeMaladie(): ?string
    {
        return $this->typeMaladie;
    }

    public function setTypeMaladie(string $typeMaladie): self
    {
        $this->typeMaladie = $typeMaladie;

        return $this;
    }

    public function getChargeTrv(): ?string
    {
        return $this->chargeTrv;
    }

    public function setChargeTrv(string $chargeTrv): self
    {
        $this->chargeTrv = $chargeTrv;

        return $this;
    }

    public function getMaladieGrave(): ?string
    {
        return $this->maladieGrave;
    }

    public function setMaladieGrave(string $maladieGrave): self
    {
        $this->maladieGrave = $maladieGrave;

        return $this;
    }

    public function getEndicape(): ?string
    {
        return $this->endicape;
    }

    public function setEndicape(string $endicape): self
    {
        $this->endicape = $endicape;

        return $this;
    }

    public function getTypeEndicape(): ?string
    {
        return $this->typeEndicape;
    }

    public function setTypeEndicape(string $typeEndicape): self
    {
        $this->typeEndicape = $typeEndicape;

        return $this;
    }

    public function getSuivie(): ?string
    {
        return $this->suivie;
    }

    public function setSuivie(string $suivie): self
    {
        $this->suivie = $suivie;

        return $this;
    }

    public function getChargeFixe(): ?string
    {
        return $this->chargeFixe;
    }

    public function setChargeFixe(string $chargeFixe): self
    {
        $this->chargeFixe = $chargeFixe;

        return $this;
    }

    public function getRemarque2(): ?string
    {
        return $this->remarque2;
    }

    public function setRemarque2(string $remarque2): self
    {
        $this->remarque2 = $remarque2;

        return $this;
    }

    public function getCasUrgent(): ?string
    {
        return $this->casUrgent;
    }

    public function setCasUrgent(string $casUrgent): self
    {
        $this->casUrgent = $casUrgent;

        return $this;
    }

    public function getDossier(): ?Dossier
    {
        return $this->dossier;
    }

    public function setDossier(?Dossier $dossier): self
    {
        $this->dossier = $dossier;

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures[] = $picture;
            $picture->setOrpheline($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->pictures->removeElement($picture)) {
            // set the owning side to null (unless already changed)
            if ($picture->getOrpheline() === $this) {
                $picture->setOrpheline(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getMedicalPictures(): Collection
    {
        return $this->medicalPictures;
    }

    public function addMedicalPicture(Picture $medicalPicture): self
    {
        if (!$this->medicalPictures->contains($medicalPicture)) {
            $this->medicalPictures[] = $medicalPicture;
            $medicalPicture->setOrphelineMedical($this);
        }

        return $this;
    }

    public function removeMedicalPicture(Picture $medicalPicture): self
    {
        if ($this->medicalPictures->removeElement($medicalPicture)) {
            // set the owning side to null (unless already changed)
            if ($medicalPicture->getOrphelineMedical() === $this) {
                $medicalPicture->setOrphelineMedical(null);
            }
        }

        return $this;
    }

    public function getChaussure(): ?string
    {
        return $this->chaussure;
    }

    public function setChaussure(?string $chaussure): self
    {
        $this->chaussure = $chaussure;

        return $this;
    }

    public function getChausUrgent(): ?string
    {
        return $this->chausUrgent;
    }

    public function setChausUrgent(?string $chausUrgent): self
    {
        $this->chausUrgent = $chausUrgent;

        return $this;
    }

    public function getSizeVetement(): ?string
    {
        return $this->sizeVetement;
    }

    public function setSizeVetement(string $sizeVetement): self
    {
        $this->sizeVetement = $sizeVetement;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCodeMassar(): ?string
    {
        return $this->codeMassar;
    }

    public function setCodeMassar(?string $codeMassar): self
    {
        $this->codeMassar = $codeMassar;

        return $this;
    }

    public function getPassMassar(): ?string
    {
        return $this->passMassar;
    }

    public function setPassMassar(?string $passMassar): self
    {
        $this->passMassar = $passMassar;

        return $this;
    }

    public function getParrain(): ?Parrains
    {
        return $this->parrain;
    }

    public function setParrain(?Parrains $parrain): self
    {
        $this->parrain = $parrain;

        return $this;
    }



}
