<?php

namespace App\Entity;

use App\Repository\SocialAideRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SocialAideRepository::class)
 */
class SocialAide
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $categorieVisee;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="Parrainer", mappedBy="socialAide" )
     */
    private $parrainers;

    public function __construct()
    {
        $this->parrainers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCategorieVisee(): ?string
    {
        return $this->categorieVisee;
    }

    public function setCategorieVisee(?string $categorieVisee): self
    {
        $this->categorieVisee = $categorieVisee;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Parrainer[]
     */
    public function getParrainers(): Collection
    {
        return $this->parrainers;
    }

    public function addParrainer(Parrainer $parrainer): self
    {
        if (!$this->parrainers->contains($parrainer)) {
            $this->parrainers[] = $parrainer;
            $parrainer->setSocialAide($this);
        }

        return $this;
    }

    public function removeParrainer(Parrainer $parrainer): self
    {
        if ($this->parrainers->removeElement($parrainer)) {
            // set the owning side to null (unless already changed)
            if ($parrainer->getSocialAide() === $this) {
                $parrainer->setSocialAide(null);
            }
        }

        return $this;
    }

}
