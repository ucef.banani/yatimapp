<?php

namespace App\Entity;

use App\Repository\HabitantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HabitantRepository::class)
 */
class Habitant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $relationOrpheline;

    /**
     * @ORM\Column(type="date" ,  nullable=true)
     */
    private $dateNaissance;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $metier;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $etablissement;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $sallaire;

    /**
     * @ORM\Column(type="string",  nullable=true)
     */
    private $charge;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $loisir;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $sante;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $remarque;




    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $typeMaladie;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $chargeTrv;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $maladieGrave;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $endicape;

     /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $typeEndicape;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $suivie;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $chargeFixe;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $remarque2;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $casUrgent;

    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="habitants")
     */
    private $dossier;



    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="habitant")
     */
    private $pictures;

    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="habitantMedical")
     */
    private $medicalPictures;



    public function __construct()
    {
        $this->pictures = new ArrayCollection();
        $this->medicalPictures = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getRelationOrpheline(): ?string
    {
        return $this->relationOrpheline;
    }

    public function setRelationOrpheline(string $relationOrpheline): self
    {
        $this->relationOrpheline = $relationOrpheline;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getMetier(): ?string
    {
        return $this->metier;
    }

    public function setMetier(string $metier): self
    {
        $this->metier = $metier;

        return $this;
    }

    public function getEtablissement(): ?string
    {
        return $this->etablissement;
    }

    public function setEtablissement(string $etablissement): self
    {
        $this->etablissement = $etablissement;

        return $this;
    }

    public function getSallaire(): ?string
    {
        return $this->sallaire;
    }

    public function setSallaire(string $sallaire): self
    {
        $this->sallaire = $sallaire;

        return $this;
    }

    
    public function getLoisir(): ?string
    {
        return $this->loisir;
    }

    public function setLoisir(string $loisir): self
    {
        $this->loisir = $loisir;

        return $this;
    }

    public function getSante(): ?string
    {
        return $this->sante;
    }

    public function setSante(string $sante): self
    {
        $this->sante = $sante;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getTypeMaladie(): ?string
    {
        return $this->typeMaladie;
    }

    public function setTypeMaladie(string $typeMaladie): self
    {
        $this->typeMaladie = $typeMaladie;

        return $this;
    }

    public function getChargeTrv(): ?string
    {
        return $this->chargeTrv;
    }

    public function setChargeTrv(string $chargeTrv): self
    {
        $this->chargeTrv = $chargeTrv;

        return $this;
    }

    public function getMaladieGrave(): ?string
    {
        return $this->maladieGrave;
    }

    public function setMaladieGrave(string $maladieGrave): self
    {
        $this->maladieGrave = $maladieGrave;

        return $this;
    }

    public function getEndicape(): ?string
    {
        return $this->endicape;
    }

    public function setEndicape(string $endicape): self
    {
        $this->endicape = $endicape;

        return $this;
    }

    public function getTypeEndicape(): ?string
    {
        return $this->typeEndicape;
    }

    public function setTypeEndicape(string $typeEndicape): self
    {
        $this->typeEndicape = $typeEndicape;

        return $this;
    }

    public function getSuivie(): ?string
    {
        return $this->suivie;
    }

    public function setSuivie(string $suivie): self
    {
        $this->suivie = $suivie;

        return $this;
    }

    public function getChargeFixe(): ?string
    {
        return $this->chargeFixe;
    }

    public function setChargeFixe(string $chargeFixe): self
    {
        $this->chargeFixe = $chargeFixe;

        return $this;
    }

    public function getRemarque2(): ?string
    {
        return $this->remarque2;
    }

    public function setRemarque2(string $remarque2): self
    {
        $this->remarque2 = $remarque2;

        return $this;
    }

    public function getCasUrgent(): ?string
    {
        return $this->casUrgent;
    }

    public function setCasUrgent(string $casUrgent): self
    {
        $this->casUrgent = $casUrgent;

        return $this;
    }

    public function getDossier(): ?Dossier
    {
        return $this->dossier;
    }

    public function setDossier(?Dossier $dossier): self
    {
        $this->dossier = $dossier;

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures[] = $picture;
            $picture->setHabitant($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->pictures->removeElement($picture)) {
            // set the owning side to null (unless already changed)
            if ($picture->getHabitant() === $this) {
                $picture->setHabitant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getMedicalPictures(): Collection
    {
        return $this->medicalPictures;
    }

    public function addMedicalPicture(Picture $medicalPicture): self
    {
        if (!$this->medicalPictures->contains($medicalPicture)) {
            $this->medicalPictures[] = $medicalPicture;
            $medicalPicture->setHabitantMedical($this);
        }

        return $this;
    }

    public function removeMedicalPicture(Picture $medicalPicture): self
    {
        if ($this->medicalPictures->removeElement($medicalPicture)) {
            // set the owning side to null (unless already changed)
            if ($medicalPicture->getHabitantMedical() === $this) {
                $medicalPicture->setHabitantMedical(null);
            }
        }

        return $this;
    }

    public function getCharge(): ?string
    {
        return $this->charge;
    }

    public function setCharge(?string $charge): self
    {
        $this->charge = $charge;

        return $this;
    }


}
