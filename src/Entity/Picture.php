<?php

namespace App\Entity;

use App\Repository\PictureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PictureRepository::class)
 */
class Picture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="pictures")
     */
    private $dossier;

    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="picturesPereMoert")
     */
    private $dossierPereMort;

    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="picturesCivil")
     */
    private $dossierCivil;

    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="picturesMarier")
     */
    private $dossierMarier;

    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="picturesVeuveCin")
     */
    private $dossierVeuveCin;

    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="picturesOrpheCin")
     */
    private $dossierOrpheCin;

    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="picturesRib")
     */
    private $dossierRib;

    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="picturesFamille")
     */
    private $dossierFamille;

    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="picturesMaison")
     */
    private $dossierMaison;

    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="picturesSignature")
     */
    private $dossierSinuature;




    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="hebergementPictures")
     */
    private $dossierHebergement;

    /**
     * @ORM\ManyToOne(targetEntity="Orpheline", inversedBy="Pictures")
     */
    private $orpheline;

    /**
     * @ORM\ManyToOne(targetEntity="Orpheline", inversedBy="medicalPictures")
     */
    private $orphelineMedical;

    /**
     * @ORM\ManyToOne(targetEntity="Habitant", inversedBy="Pictures")
     */
    private $habitant;

    /**
     * @ORM\ManyToOne(targetEntity="Habitant", inversedBy="medicalPictures")
     */
    private $habitantMedical;


    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="medicalVeuvePictures")
     */
    private $veuveMedical;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDossier(): ?Dossier
    {
        return $this->dossier;
    }

    public function setDossier(?Dossier $dossier): self
    {
        $this->dossier = $dossier;

        return $this;
    }

    public function getDossierHebergement(): ?Dossier
    {
        return $this->dossierHebergement;
    }

    public function setDossierHebergement(?Dossier $dossierHebergement): self
    {
        $this->dossierHebergement = $dossierHebergement;

        return $this;
    }

    public function getOrpheline(): ?Orpheline
    {
        return $this->orpheline;
    }

    public function setOrpheline(?Orpheline $orpheline): self
    {
        $this->orpheline = $orpheline;

        return $this;
    }

    public function getOrphelineMedical(): ?Orpheline
    {
        return $this->orphelineMedical;
    }

    public function setOrphelineMedical(?Orpheline $orphelineMedical): self
    {
        $this->orphelineMedical = $orphelineMedical;

        return $this;
    }

    public function getHabitant(): ?Habitant
    {
        return $this->habitant;
    }

    public function setHabitant(?Habitant $habitant): self
    {
        $this->habitant = $habitant;

        return $this;
    }

    public function getHabitantMedical(): ?Habitant
    {
        return $this->habitantMedical;
    }

    public function setHabitantMedical(?Habitant $habitantMedical): self
    {
        $this->habitantMedical = $habitantMedical;

        return $this;
    }

    public function getDossierPereMort(): ?Dossier
    {
        return $this->dossierPereMort;
    }

    public function setDossierPereMort(?Dossier $dossierPereMort): self
    {
        $this->dossierPereMort = $dossierPereMort;

        return $this;
    }

    public function getDossierCivil(): ?Dossier
    {
        return $this->dossierCivil;
    }

    public function setDossierCivil(?Dossier $dossierCivil): self
    {
        $this->dossierCivil = $dossierCivil;

        return $this;
    }

    public function getDossierMarier(): ?Dossier
    {
        return $this->dossierMarier;
    }

    public function setDossierMarier(?Dossier $dossierMarier): self
    {
        $this->dossierMarier = $dossierMarier;

        return $this;
    }

    public function getDossierVeuveCin(): ?Dossier
    {
        return $this->dossierVeuveCin;
    }

    public function setDossierVeuveCin(?Dossier $dossierVeuveCin): self
    {
        $this->dossierVeuveCin = $dossierVeuveCin;

        return $this;
    }

    public function getDossierOrpheCin(): ?Dossier
    {
        return $this->dossierOrpheCin;
    }

    public function setDossierOrpheCin(?Dossier $dossierOrpheCin): self
    {
        $this->dossierOrpheCin = $dossierOrpheCin;

        return $this;
    }

    public function getDossierRib(): ?Dossier
    {
        return $this->dossierRib;
    }

    public function setDossierRib(?Dossier $dossierRib): self
    {
        $this->dossierRib = $dossierRib;

        return $this;
    }

    public function getDossierFamille(): ?Dossier
    {
        return $this->dossierFamille;
    }

    public function setDossierFamille(?Dossier $dossierFamille): self
    {
        $this->dossierFamille = $dossierFamille;

        return $this;
    }

    public function getDossierMaison(): ?Dossier
    {
        return $this->dossierMaison;
    }

    public function setDossierMaison(?Dossier $dossierMaison): self
    {
        $this->dossierMaison = $dossierMaison;

        return $this;
    }

    public function getDossierSinuature(): ?Dossier
    {
        return $this->dossierSinuature;
    }

    public function setDossierSinuature(?Dossier $dossierSinuature): self
    {
        $this->dossierSinuature = $dossierSinuature;

        return $this;
    }

    public function getVeuveMedical(): ?Dossier
    {
        return $this->veuveMedical;
    }

    public function setVeuveMedical(?Dossier $veuveMedical): self
    {
        $this->veuveMedical = $veuveMedical;

        return $this;
    }



}
