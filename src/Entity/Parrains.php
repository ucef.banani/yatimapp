<?php

namespace App\Entity;

use App\Repository\ParrainsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParrainsRepository::class)
 */
class Parrains
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tele;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mediateur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prix;

    /**
     * @ORM\Column(type="date",  nullable=true)
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="date",  nullable=true)
     */
    private $dateFin;

    /**
     * @ORM\Column(type="date",  nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $problemePause;

    /**
     * @ORM\OneToMany(targetEntity="Dossier", mappedBy="parrain")
     */
    private $dossier;

    /**
     * @ORM\OneToMany(targetEntity="Orpheline", mappedBy="parrain")
     */
    private $orpheline;

    /**
     * @ORM\OneToMany(targetEntity="ParrainPay", mappedBy="parrain")
     */
    private $parrainsPay;


    public function __construct()
    {
        $this->dossier = new ArrayCollection();
        $this->parrainsPay = new ArrayCollection();
        $this->orpheline = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getTele(): ?string
    {
        return $this->tele;
    }

    public function setTele(?string $tele): self
    {
        $this->tele = $tele;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getMediateur(): ?string
    {
        return $this->mediateur;
    }

    public function setMediateur(?string $mediateur): self
    {
        $this->mediateur = $mediateur;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(?string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
 

    /**
     * @return Collection|Dossier[]
     */
    public function getDossier(): Collection
    {
        return $this->dossier;
    }

    public function addDossier(Dossier $dossier): self
    {
        if (!$this->dossier->contains($dossier)) {
            $this->dossier[] = $dossier;
            $dossier->setParrain($this);
        }

        return $this;
    }

    public function removeDossier(Dossier $dossier): self
    {
        if ($this->dossier->removeElement($dossier)) {
            // set the owning side to null (unless already changed)
            if ($dossier->getParrain() === $this) {
                $dossier->setParrain(null);
            }
        }

        return $this;
    }

    public function getProblemePause(): ?string
    {
        return $this->problemePause;
    }

    public function setProblemePause(?string $problemePause): self
    {
        $this->problemePause = $problemePause;

        return $this;
    }

    /**
     * @return Collection|ParrainPay[]
     */
    public function getParrainsPay(): Collection
    {
        return $this->parrainsPay;
    }

    public function addParrainsPay(ParrainPay $parrainsPay): self
    {
        if (!$this->parrainsPay->contains($parrainsPay)) {
            $this->parrainsPay[] = $parrainsPay;
            $parrainsPay->setParrain($this);
        }

        return $this;
    }

    public function removeParrainsPay(ParrainPay $parrainsPay): self
    {
        if ($this->parrainsPay->removeElement($parrainsPay)) {
            // set the owning side to null (unless already changed)
            if ($parrainsPay->getParrain() === $this) {
                $parrainsPay->setParrain(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Orpheline[]
     */
    public function getOrpheline(): Collection
    {
        return $this->orpheline;
    }

    public function addOrpheline(Orpheline $orpheline): self
    {
        if (!$this->orpheline->contains($orpheline)) {
            $this->orpheline[] = $orpheline;
            $orpheline->setParrain($this);
        }

        return $this;
    }

    public function removeOrpheline(Orpheline $orpheline): self
    {
        if ($this->orpheline->removeElement($orpheline)) {
            // set the owning side to null (unless already changed)
            if ($orpheline->getParrain() === $this) {
                $orpheline->setParrain(null);
            }
        }

        return $this;
    }
 


}
