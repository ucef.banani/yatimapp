<?php

namespace App\Entity;

use App\Repository\DossierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DossierRepository::class)
 */
class Dossier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $code;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $familleNom;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $region;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $commune;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $tel1;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $tel2;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $tel3;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $nationalite;

/*الملف الاجتماعي*/

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $aideSociale;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $montantAideSociale;


    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $soutien;


    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $montantSoutien;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $soutienAssosiation;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $montantSoutienAssosiation;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $aucunSoutien;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $autreSoutien;


    /*الممتلكات الموروثة :*/


    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $louerTerrain;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $montantLouerTerrain;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $louerAppartement;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $montantLouerAppartement;


    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $louerMagasins;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $montantLlouerMagasins;


    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $terreAgricole;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $montantTerreAgricole;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $mouton;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $numberMouton;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $aucunProprietes;

/*مصاريف السكن*/
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $hypotheque;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $montantHypotheque;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $louerHebergement;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $montanyLouerHebergement;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $AucunHebergement;

/*مصاريف الماء والكهرباء*/

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $chargeEau;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $montantChargeEau;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $chargeElictricite;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $montantChargeElictricite;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $aucunchargeEau;

    /*المساعدة الخارجية :*/
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $aidez;


    /*وضعية السكن*/

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $typeHebergement;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $urgentTypeHebergement;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $adjectif;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */

    private $urgentAdjectif;


    /*حالة البناء*/
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $construction;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $urgentConstruction;

    /**
     * @ORM\Column(type="json_array",  nullable=true)
     */
    private $equipement;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $urgentEquipement;


        /*حالة الاتات*/
        /*tv*/
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $tvBien;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $tvFible;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $tvNull;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $tvBesoin;

    /*frigo **/


    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $frigoBien;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $frigoFible;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $frigoNull;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $frigoBesoin;

    /*cousine **/

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $couisinBien;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $couisinFible;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $couisinNull;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $couisinBesoin;

    /*machine **/

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $machineBien;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $machineFible;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $machineNull;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $machineBesoin;

    /*vetment **/

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $vetmentBien;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $vetmentFible;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $vetmentNull;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $vetmentBesoin;

    /*matelas **/

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $matelasBien;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $matelasFible;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $matelasNull;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $matelasBesoin;

    /*couvertures **/

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $couverturesBien;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $couverturesFible;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $couverturesNull;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $couverturesBesoin;


    /* info pere/*/

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $pereNom;
    /**
     * @ORM\Column(type="date", nullable=true))
     */
    private $pereDatenissance;
    /**
     * @ORM\Column(type="date",  nullable=true))
     */
    private $pereDatemort;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $pereCin;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $pereCauseMort;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $peretravail;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $pereEmployeur;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $pereRetrait;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $pereAccident;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $pereRemarque;


    /* info veuve/*/

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $veuve;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $veuveCin;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $orphelinRelation;
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $veuveDateNissance;
    /**
     * @ORM\Column(type="string" , nullable=true)
     */
    private $veuveScolaire;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $veuveTraville;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $veuveEmployeur;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $veuveSalaire;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $veuveCharge;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $veuveActivite;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $veuveType;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $veuveSante;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $veuveBank;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $veuveVetment;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $veuveChaussure;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $veuveUrgent;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $veuveRemarque;



    /* sante veuve*/
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $typeMaladie;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $chargeTrv;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $maladieGrave;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $endicape;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $typeEndicape;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $suivie;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $chargeFixe;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $remarque2;
    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $casUrgent;






    /*almo3il info soutien**/

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $soutienNom;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $soutienRelationOrpheline;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $soutienTravail;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $soutienType;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $soutienMontant;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $avecLaFamille;



    /**
     * @ORM\OneToMany(targetEntity="Orpheline", mappedBy="dossier", cascade={"remove"})
     */
    private $orphelines;


    /**
     * @ORM\OneToMany(targetEntity="Habitant", mappedBy="dossier", cascade={"remove"})
     */
    private $habitants;



    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="dossier", cascade={"remove"})
     */
    private $pictures;

    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="dossierHebergement", cascade={"remove"})
     */
    private $hebergementPictures;



    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="dossierPereMort", cascade={"remove"})
     */
    private $picturesPereMoert;

    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="dossierCivil", cascade={"remove"})
     */
    private $picturesCivil;

    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="dossierMarier", cascade={"remove"})
     */
    private $picturesMarier;

    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="dossierVeuveCin", cascade={"remove"})
     */
    private $picturesVeuveCin;

    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="dossierOrpheCin", cascade={"remove"})
     */
    private $picturesOrpheCin;

    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="dossierRib", cascade={"remove"})
     */
    private $picturesRib;

    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="dossierFamille", cascade={"remove"})
     */
    private $picturesFamille;

    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="dossierMaison" , cascade={"remove"})
     */
    private $picturesMaison;

    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="dossierSinuature" , cascade={"remove"})
     */
    private $picturesSignature;


    /**
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="veuveMedical" , cascade={"remove"})
     */
    private $medicalVeuvePictures;

    /**
     * @ORM\OneToMany(targetEntity="Parrainer", mappedBy="dossier" , cascade={"remove"})
     */
    private $parrainers;

    /**
     * @ORM\ManyToOne(targetEntity="Parrains", inversedBy="dossiers")
     */
    private $parrain;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="dossiers"
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="ParrainPay", mappedBy="dossier" , cascade={"remove"})
     */
    private $parrainsPay;





    public function __construct()
    {
        $this->orphelines = new ArrayCollection();
        $this->habitants = new ArrayCollection();
        $this->pictures = new ArrayCollection();
        $this->hebergementPictures = new ArrayCollection();
        $this->picturesPereMoert = new ArrayCollection();
        $this->picturesCivil = new ArrayCollection();
        $this->picturesMarier = new ArrayCollection();
        $this->picturesVeuveCin = new ArrayCollection();
        $this->picturesOrpheCin = new ArrayCollection();
        $this->picturesRib = new ArrayCollection();
        $this->picturesFamille = new ArrayCollection();
        $this->picturesMaison = new ArrayCollection();
        $this->picturesSignature = new ArrayCollection();
        $this->medicalVeuvePictures = new ArrayCollection();
        $this->parrainers = new ArrayCollection();
        $this->parrainsPay = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getFamilleNom(): ?string
    {
        return $this->familleNom;
    }

    public function setFamilleNom(?string $familleNom): self
    {
        $this->familleNom = $familleNom;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getCommune(): ?string
    {
        return $this->commune;
    }

    public function setCommune(?string $commune): self
    {
        $this->commune = $commune;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getTel1(): ?string
    {
        return $this->tel1;
    }

    public function setTel1(?string $tel1): self
    {
        $this->tel1 = $tel1;

        return $this;
    }

    public function getTel2(): ?string
    {
        return $this->tel2;
    }

    public function setTel2(?string $tel2): self
    {
        $this->tel2 = $tel2;

        return $this;
    }

    public function getTel3(): ?string
    {
        return $this->tel3;
    }

    public function setTel3(?string $tel3): self
    {
        $this->tel3 = $tel3;

        return $this;
    }

    public function getNationalite(): ?string
    {
        return $this->nationalite;
    }

    public function setNationalite(?string $nationalite): self
    {
        $this->nationalite = $nationalite;

        return $this;
    }

    public function getAideSociale(): ?string
    {
        return $this->aideSociale;
    }

    public function setAideSociale(?string $aideSociale): self
    {
        $this->aideSociale = $aideSociale;

        return $this;
    }

    public function getMontantAideSociale(): ?string
    {
        return $this->montantAideSociale;
    }

    public function setMontantAideSociale(?string $montantAideSociale): self
    {
        $this->montantAideSociale = $montantAideSociale;

        return $this;
    }

    public function getSoutien(): ?string
    {
        return $this->soutien;
    }

    public function setSoutien(?string $soutien): self
    {
        $this->soutien = $soutien;

        return $this;
    }

    public function getMontantSoutien(): ?string
    {
        return $this->montantSoutien;
    }

    public function setMontantSoutien(?string $montantSoutien): self
    {
        $this->montantSoutien = $montantSoutien;

        return $this;
    }

    public function getSoutienAssosiation(): ?string
    {
        return $this->soutienAssosiation;
    }

    public function setSoutienAssosiation(?string $soutienAssosiation): self
    {
        $this->soutienAssosiation = $soutienAssosiation;

        return $this;
    }

    public function getMontantSoutienAssosiation(): ?string
    {
        return $this->montantSoutienAssosiation;
    }

    public function setMontantSoutienAssosiation(?string $montantSoutienAssosiation): self
    {
        $this->montantSoutienAssosiation = $montantSoutienAssosiation;

        return $this;
    }

    public function getAucunSoutien(): ?string
    {
        return $this->aucunSoutien;
    }

    public function setAucunSoutien(?string $aucunSoutien): self
    {
        $this->aucunSoutien = $aucunSoutien;

        return $this;
    }

    public function getAutreSoutien(): ?string
    {
        return $this->autreSoutien;
    }

    public function setAutreSoutien(?string $autreSoutien): self
    {
        $this->autreSoutien = $autreSoutien;

        return $this;
    }

    public function getLouerTerrain(): ?string
    {
        return $this->louerTerrain;
    }

    public function setLouerTerrain(?string $louerTerrain): self
    {
        $this->louerTerrain = $louerTerrain;

        return $this;
    }

    public function getMontantLouerTerrain(): ?string
    {
        return $this->montantLouerTerrain;
    }

    public function setMontantLouerTerrain(?string $montantLouerTerrain): self
    {
        $this->montantLouerTerrain = $montantLouerTerrain;

        return $this;
    }

    public function getLouerAppartement(): ?string
    {
        return $this->louerAppartement;
    }

    public function setLouerAppartement(?string $louerAppartement): self
    {
        $this->louerAppartement = $louerAppartement;

        return $this;
    }

    public function getMontantLouerAppartement(): ?string
    {
        return $this->montantLouerAppartement;
    }

    public function setMontantLouerAppartement(?string $montantLouerAppartement): self
    {
        $this->montantLouerAppartement = $montantLouerAppartement;

        return $this;
    }

    public function getTerreAgricole(): ?string
    {
        return $this->terreAgricole;
    }

    public function setTerreAgricole(?string $terreAgricole): self
    {
        $this->terreAgricole = $terreAgricole;

        return $this;
    }

    public function getMontantTerreAgricole(): ?string
    {
        return $this->montantTerreAgricole;
    }

    public function setMontantTerreAgricole(?string $montantTerreAgricole): self
    {
        $this->montantTerreAgricole = $montantTerreAgricole;

        return $this;
    }

    public function getMouton(): ?string
    {
        return $this->mouton;
    }

    public function setMouton(?string $mouton): self
    {
        $this->mouton = $mouton;

        return $this;
    }

    public function getNumberMouton(): ?string
    {
        return $this->numberMouton;
    }

    public function setNumberMouton(?string $numberMouton): self
    {
        $this->numberMouton = $numberMouton;

        return $this;
    }

    public function getAucunProprietes(): ?string
    {
        return $this->aucunProprietes;
    }

    public function setAucunProprietes(?string $aucunProprietes): self
    {
        $this->aucunProprietes = $aucunProprietes;

        return $this;
    }

    public function getHypotheque(): ?string
    {
        return $this->hypotheque;
    }

    public function setHypotheque(?string $hypotheque): self
    {
        $this->hypotheque = $hypotheque;

        return $this;
    }

    public function getMontantHypotheque(): ?string
    {
        return $this->montantHypotheque;
    }

    public function setMontantHypotheque(?string $montantHypotheque): self
    {
        $this->montantHypotheque = $montantHypotheque;

        return $this;
    }

    public function getLouerHebergement(): ?string
    {
        return $this->louerHebergement;
    }

    public function setLouerHebergement(?string $louerHebergement): self
    {
        $this->louerHebergement = $louerHebergement;

        return $this;
    }

    public function getMontanyLouerHebergement(): ?string
    {
        return $this->montanyLouerHebergement;
    }

    public function setMontanyLouerHebergement(?string $montanyLouerHebergement): self
    {
        $this->montanyLouerHebergement = $montanyLouerHebergement;

        return $this;
    }

    public function getAucunHebergement(): ?string
    {
        return $this->AucunHebergement;
    }

    public function setAucunHebergement(?string $AucunHebergement): self
    {
        $this->AucunHebergement = $AucunHebergement;

        return $this;
    }

    public function getChargeEau(): ?string
    {
        return $this->chargeEau;
    }

    public function setChargeEau(?string $chargeEau): self
    {
        $this->chargeEau = $chargeEau;

        return $this;
    }

    public function getMontantChargeEau(): ?string
    {
        return $this->montantChargeEau;
    }

    public function setMontantChargeEau(?string $montantChargeEau): self
    {
        $this->montantChargeEau = $montantChargeEau;

        return $this;
    }

    public function getChargeElictricite(): ?string
    {
        return $this->chargeElictricite;
    }

    public function setChargeElictricite(?string $chargeElictricite): self
    {
        $this->chargeElictricite = $chargeElictricite;

        return $this;
    }

    public function getMontantChargeElictricite(): ?string
    {
        return $this->montantChargeElictricite;
    }

    public function setMontantChargeElictricite(?string $montantChargeElictricite): self
    {
        $this->montantChargeElictricite = $montantChargeElictricite;

        return $this;
    }

    public function getAucunchargeEau(): ?string
    {
        return $this->aucunchargeEau;
    }

    public function setAucunchargeEau(?string $aucunchargeEau): self
    {
        $this->aucunchargeEau = $aucunchargeEau;

        return $this;
    }

    public function getAidez(): ?string
    {
        return $this->aidez;
    }

    public function setAidez(?string $aidez): self
    {
        $this->aidez = $aidez;

        return $this;
    }

    public function getTypeHebergement(): ?string
    {
        return $this->typeHebergement;
    }

    public function setTypeHebergement(?string $typeHebergement): self
    {
        $this->typeHebergement = $typeHebergement;

        return $this;
    }

    public function getUrgentTypeHebergement(): ?string
    {
        return $this->urgentTypeHebergement;
    }

    public function setUrgentTypeHebergement(?string $urgentTypeHebergement): self
    {
        $this->urgentTypeHebergement = $urgentTypeHebergement;

        return $this;
    }

    public function getAdjectif(): ?string
    {
        return $this->adjectif;
    }

    public function setAdjectif(?string $adjectif): self
    {
        $this->adjectif = $adjectif;

        return $this;
    }

    public function getUrgentAdjectif(): ?string
    {
        return $this->urgentAdjectif;
    }

    public function setUrgentAdjectif(?string $urgentAdjectif): self
    {
        $this->urgentAdjectif = $urgentAdjectif;

        return $this;
    }

    public function getConstruction(): ?string
    {
        return $this->construction;
    }

    public function setConstruction(?string $construction): self
    {
        $this->construction = $construction;

        return $this;
    }

    public function getUrgentConstruction(): ?string
    {
        return $this->urgentConstruction;
    }

    public function setUrgentConstruction(?string $urgentConstruction): self
    {
        $this->urgentConstruction = $urgentConstruction;

        return $this;
    }



    public function getUrgentEquipement(): ?string
    {
        return $this->urgentEquipement;
    }

    public function setUrgentEquipement(?string $urgentEquipement): self
    {
        $this->urgentEquipement = $urgentEquipement;

        return $this;
    }

    public function getTvBien(): ?string
    {
        return $this->tvBien;
    }

    public function setTvBien(?string $tvBien): self
    {
        $this->tvBien = $tvBien;

        return $this;
    }

    public function getTvFible(): ?string
    {
        return $this->tvFible;
    }

    public function setTvFible(?string $tvFible): self
    {
        $this->tvFible = $tvFible;

        return $this;
    }

    public function getTvNull(): ?string
    {
        return $this->tvNull;
    }

    public function setTvNull(?string $tvNull): self
    {
        $this->tvNull = $tvNull;

        return $this;
    }

    public function getTvBesoin(): ?string
    {
        return $this->tvBesoin;
    }

    public function setTvBesoin(?string $tvBesoin): self
    {
        $this->tvBesoin = $tvBesoin;

        return $this;
    }

    public function getFrigoBien(): ?string
    {
        return $this->frigoBien;
    }

    public function setFrigoBien(?string $frigoBien): self
    {
        $this->frigoBien = $frigoBien;

        return $this;
    }

    public function getFrigoFible(): ?string
    {
        return $this->frigoFible;
    }

    public function setFrigoFible(?string $frigoFible): self
    {
        $this->frigoFible = $frigoFible;

        return $this;
    }

    public function getFrigoNull(): ?string
    {
        return $this->frigoNull;
    }

    public function setFrigoNull(?string $frigoNull): self
    {
        $this->frigoNull = $frigoNull;

        return $this;
    }

    public function getFrigoBesoin(): ?string
    {
        return $this->frigoBesoin;
    }

    public function setFrigoBesoin(?string $frigoBesoin): self
    {
        $this->frigoBesoin = $frigoBesoin;

        return $this;
    }

    public function getCouisinBien(): ?string
    {
        return $this->couisinBien;
    }

    public function setCouisinBien(?string $couisinBien): self
    {
        $this->couisinBien = $couisinBien;

        return $this;
    }

    public function getCouisinFible(): ?string
    {
        return $this->couisinFible;
    }

    public function setCouisinFible(?string $couisinFible): self
    {
        $this->couisinFible = $couisinFible;

        return $this;
    }

    public function getCouisinNull(): ?string
    {
        return $this->couisinNull;
    }

    public function setCouisinNull(?string $couisinNull): self
    {
        $this->couisinNull = $couisinNull;

        return $this;
    }

    public function getCouisinBesoin(): ?string
    {
        return $this->couisinBesoin;
    }

    public function setCouisinBesoin(?string $couisinBesoin): self
    {
        $this->couisinBesoin = $couisinBesoin;

        return $this;
    }

    public function getMachineBien(): ?string
    {
        return $this->machineBien;
    }

    public function setMachineBien(?string $machineBien): self
    {
        $this->machineBien = $machineBien;

        return $this;
    }

    public function getMachineFible(): ?string
    {
        return $this->machineFible;
    }

    public function setMachineFible(?string $machineFible): self
    {
        $this->machineFible = $machineFible;

        return $this;
    }

    public function getMachineNull(): ?string
    {
        return $this->machineNull;
    }

    public function setMachineNull(?string $machineNull): self
    {
        $this->machineNull = $machineNull;

        return $this;
    }

    public function getMachineBesoin(): ?string
    {
        return $this->machineBesoin;
    }

    public function setMachineBesoin(?string $machineBesoin): self
    {
        $this->machineBesoin = $machineBesoin;

        return $this;
    }

    public function getVetmentBien(): ?string
    {
        return $this->vetmentBien;
    }

    public function setVetmentBien(?string $vetmentBien): self
    {
        $this->vetmentBien = $vetmentBien;

        return $this;
    }

    public function getVetmentFible(): ?string
    {
        return $this->vetmentFible;
    }

    public function setVetmentFible(?string $vetmentFible): self
    {
        $this->vetmentFible = $vetmentFible;

        return $this;
    }

    public function getVetmentNull(): ?string
    {
        return $this->vetmentNull;
    }

    public function setVetmentNull(?string $vetmentNull): self
    {
        $this->vetmentNull = $vetmentNull;

        return $this;
    }

    public function getVetmentBesoin(): ?string
    {
        return $this->vetmentBesoin;
    }

    public function setVetmentBesoin(?string $vetmentBesoin): self
    {
        $this->vetmentBesoin = $vetmentBesoin;

        return $this;
    }

    public function getMatelasBien(): ?string
    {
        return $this->matelasBien;
    }

    public function setMatelasBien(?string $matelasBien): self
    {
        $this->matelasBien = $matelasBien;

        return $this;
    }

    public function getMatelasFible(): ?string
    {
        return $this->matelasFible;
    }

    public function setMatelasFible(?string $matelasFible): self
    {
        $this->matelasFible = $matelasFible;

        return $this;
    }

    public function getMatelasNull(): ?string
    {
        return $this->matelasNull;
    }

    public function setMatelasNull(?string $matelasNull): self
    {
        $this->matelasNull = $matelasNull;

        return $this;
    }

    public function getMatelasBesoin(): ?string
    {
        return $this->matelasBesoin;
    }

    public function setMatelasBesoin(?string $matelasBesoin): self
    {
        $this->matelasBesoin = $matelasBesoin;

        return $this;
    }

    public function getCouverturesBien(): ?string
    {
        return $this->couverturesBien;
    }

    public function setCouverturesBien(?string $couverturesBien): self
    {
        $this->couverturesBien = $couverturesBien;

        return $this;
    }

    public function getCouverturesFible(): ?string
    {
        return $this->couverturesFible;
    }

    public function setCouverturesFible(?string $couverturesFible): self
    {
        $this->couverturesFible = $couverturesFible;

        return $this;
    }

    public function getCouverturesNull(): ?string
    {
        return $this->couverturesNull;
    }

    public function setCouverturesNull(?string $couverturesNull): self
    {
        $this->couverturesNull = $couverturesNull;

        return $this;
    }

    public function getCouverturesBesoin(): ?string
    {
        return $this->couverturesBesoin;
    }

    public function setCouverturesBesoin(?string $couverturesBesoin): self
    {
        $this->couverturesBesoin = $couverturesBesoin;

        return $this;
    }

    public function getPereNom(): ?string
    {
        return $this->pereNom;
    }

    public function setPereNom(?string $pereNom): self
    {
        $this->pereNom = $pereNom;

        return $this;
    }

    public function getPereDatenissance(): ?\DateTimeInterface
    {
        return $this->pereDatenissance;
    }

    public function setPereDatenissance(?\DateTimeInterface $pereDatenissance): self
    {
        $this->pereDatenissance = $pereDatenissance;

        return $this;
    }

    public function getPereDatemort(): ?\DateTimeInterface
    {
        return $this->pereDatemort;
    }

    public function setPereDatemort(?\DateTimeInterface $pereDatemort): self
    {
        $this->pereDatemort = $pereDatemort;

        return $this;
    }

    public function getPereCin(): ?string
    {
        return $this->pereCin;
    }

    public function setPereCin(?string $pereCin): self
    {
        $this->pereCin = $pereCin;

        return $this;
    }

    public function getPereCauseMort(): ?string
    {
        return $this->pereCauseMort;
    }

    public function setPereCauseMort(?string $pereCauseMort): self
    {
        $this->pereCauseMort = $pereCauseMort;

        return $this;
    }

    public function getPeretravail(): ?string
    {
        return $this->peretravail;
    }

    public function setPeretravail(?string $peretravail): self
    {
        $this->peretravail = $peretravail;

        return $this;
    }

    public function getPereEmployeur(): ?string
    {
        return $this->pereEmployeur;
    }

    public function setPereEmployeur(?string $pereEmployeur): self
    {
        $this->pereEmployeur = $pereEmployeur;

        return $this;
    }

    public function getPereRetrait(): ?string
    {
        return $this->pereRetrait;
    }

    public function setPereRetrait(?string $pereRetrait): self
    {
        $this->pereRetrait = $pereRetrait;

        return $this;
    }

    public function getPereAccident(): ?string
    {
        return $this->pereAccident;
    }

    public function setPereAccident(?string $pereAccident): self
    {
        $this->pereAccident = $pereAccident;

        return $this;
    }

    public function getPereRemarque(): ?string
    {
        return $this->pereRemarque;
    }

    public function setPereRemarque(?string $pereRemarque): self
    {
        $this->pereRemarque = $pereRemarque;

        return $this;
    }

    public function getVeuve(): ?string
    {
        return $this->veuve;
    }

    public function setVeuve(?string $veuve): self
    {
        $this->veuve = $veuve;

        return $this;
    }

    public function getVeuveCin(): ?string
    {
        return $this->veuveCin;
    }

    public function setVeuveCin(?string $veuveCin): self
    {
        $this->veuveCin = $veuveCin;

        return $this;
    }

    public function getOrphelinRelation(): ?string
    {
        return $this->orphelinRelation;
    }

    public function setOrphelinRelation(?string $orphelinRelation): self
    {
        $this->orphelinRelation = $orphelinRelation;

        return $this;
    }

    public function getVeuveDateNissance(): ?\DateTimeInterface
    {
        return $this->veuveDateNissance;
    }

    public function setVeuveDateNissance(?\DateTimeInterface $veuveDateNissance): self
    {
        $this->veuveDateNissance = $veuveDateNissance;

        return $this;
    }

    public function getVeuveScolaire(): ?string
    {
        return $this->veuveScolaire;
    }

    public function setVeuveScolaire(?string $veuveScolaire): self
    {
        $this->veuveScolaire = $veuveScolaire;

        return $this;
    }

    public function getVeuveTraville(): ?string
    {
        return $this->veuveTraville;
    }

    public function setVeuveTraville(?string $veuveTraville): self
    {
        $this->veuveTraville = $veuveTraville;

        return $this;
    }

    public function getVeuveEmployeur(): ?string
    {
        return $this->veuveEmployeur;
    }

    public function setVeuveEmployeur(?string $veuveEmployeur): self
    {
        $this->veuveEmployeur = $veuveEmployeur;

        return $this;
    }

    public function getVeuveSalaire(): ?string
    {
        return $this->veuveSalaire;
    }

    public function setVeuveSalaire(?string $veuveSalaire): self
    {
        $this->veuveSalaire = $veuveSalaire;

        return $this;
    }

    public function getVeuveCharge(): ?string
    {
        return $this->veuveCharge;
    }

    public function setVeuveCharge(?string $veuveCharge): self
    {
        $this->veuveCharge = $veuveCharge;

        return $this;
    }

    public function getVeuveActivite(): ?string
    {
        return $this->veuveActivite;
    }

    public function setVeuveActivite(?string $veuveActivite): self
    {
        $this->veuveActivite = $veuveActivite;

        return $this;
    }

    public function getVeuveType(): ?string
    {
        return $this->veuveType;
    }

    public function setVeuveType(?string $veuveType): self
    {
        $this->veuveType = $veuveType;

        return $this;
    }

    public function getVeuveSante(): ?string
    {
        return $this->veuveSante;
    }

    public function setVeuveSante(?string $veuveSante): self
    {
        $this->veuveSante = $veuveSante;

        return $this;
    }

    public function getVeuveBank(): ?string
    {
        return $this->veuveBank;
    }

    public function setVeuveBank(?string $veuveBank): self
    {
        $this->veuveBank = $veuveBank;

        return $this;
    }

    public function getVeuveVetment(): ?string
    {
        return $this->veuveVetment;
    }

    public function setVeuveVetment(?string $veuveVetment): self
    {
        $this->veuveVetment = $veuveVetment;

        return $this;
    }

    public function getVeuveChaussure(): ?string
    {
        return $this->veuveChaussure;
    }

    public function setVeuveChaussure(?string $veuveChaussure): self
    {
        $this->veuveChaussure = $veuveChaussure;

        return $this;
    }

    public function getVeuveUrgent(): ?string
    {
        return $this->veuveUrgent;
    }

    public function setVeuveUrgent(?string $veuveUrgent): self
    {
        $this->veuveUrgent = $veuveUrgent;

        return $this;
    }

    public function getVeuveRemarque(): ?string
    {
        return $this->veuveRemarque;
    }

    public function setVeuveRemarque(?string $veuveRemarque): self
    {
        $this->veuveRemarque = $veuveRemarque;

        return $this;
    }

    public function getTypeMaladie(): ?string
    {
        return $this->typeMaladie;
    }

    public function setTypeMaladie(?string $typeMaladie): self
    {
        $this->typeMaladie = $typeMaladie;

        return $this;
    }

    public function getChargeTrv(): ?string
    {
        return $this->chargeTrv;
    }

    public function setChargeTrv(?string $chargeTrv): self
    {
        $this->chargeTrv = $chargeTrv;

        return $this;
    }

    public function getMaladieGrave(): ?string
    {
        return $this->maladieGrave;
    }

    public function setMaladieGrave(?string $maladieGrave): self
    {
        $this->maladieGrave = $maladieGrave;

        return $this;
    }

    public function getEndicape(): ?string
    {
        return $this->endicape;
    }

    public function setEndicape(?string $endicape): self
    {
        $this->endicape = $endicape;

        return $this;
    }

    public function getTypeEndicape(): ?string
    {
        return $this->typeEndicape;
    }

    public function setTypeEndicape(?string $typeEndicape): self
    {
        $this->typeEndicape = $typeEndicape;

        return $this;
    }

    public function getSuivie(): ?string
    {
        return $this->suivie;
    }

    public function setSuivie(?string $suivie): self
    {
        $this->suivie = $suivie;

        return $this;
    }

    public function getChargeFixe(): ?string
    {
        return $this->chargeFixe;
    }

    public function setChargeFixe(?string $chargeFixe): self
    {
        $this->chargeFixe = $chargeFixe;

        return $this;
    }

    public function getRemarque2(): ?string
    {
        return $this->remarque2;
    }

    public function setRemarque2(?string $remarque2): self
    {
        $this->remarque2 = $remarque2;

        return $this;
    }

    public function getCasUrgent(): ?string
    {
        return $this->casUrgent;
    }

    public function setCasUrgent(?string $casUrgent): self
    {
        $this->casUrgent = $casUrgent;

        return $this;
    }

    public function getSoutienNom(): ?string
    {
        return $this->soutienNom;
    }

    public function setSoutienNom(?string $soutienNom): self
    {
        $this->soutienNom = $soutienNom;

        return $this;
    }

    public function getSoutienRelationOrpheline(): ?string
    {
        return $this->soutienRelationOrpheline;
    }

    public function setSoutienRelationOrpheline(?string $soutienRelationOrpheline): self
    {
        $this->soutienRelationOrpheline = $soutienRelationOrpheline;

        return $this;
    }

    public function getSoutienTravail(): ?string
    {
        return $this->soutienTravail;
    }

    public function setSoutienTravail(?string $soutienTravail): self
    {
        $this->soutienTravail = $soutienTravail;

        return $this;
    }

    public function getSoutienType(): ?string
    {
        return $this->soutienType;
    }

    public function setSoutienType(?string $soutienType): self
    {
        $this->soutienType = $soutienType;

        return $this;
    }

    /**
     * @return Collection|Orpheline[]
     */
    public function getOrphelines(): Collection
    {
        return $this->orphelines;
    }

    public function addOrpheline(Orpheline $orpheline): self
    {
        if (!$this->orphelines->contains($orpheline)) {
            $this->orphelines[] = $orpheline;
            $orpheline->setDossier($this);
        }

        return $this;
    }

    public function removeOrpheline(Orpheline $orpheline): self
    {
        if ($this->orphelines->removeElement($orpheline)) {
            // set the owning side to null (unless already changed)
            if ($orpheline->getDossier() === $this) {
                $orpheline->setDossier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Habitant[]
     */
    public function getHabitants(): Collection
    {
        return $this->habitants;
    }

    public function addHabitant(Habitant $habitant): self
    {
        if (!$this->habitants->contains($habitant)) {
            $this->habitants[] = $habitant;
            $habitant->setDossier($this);
        }

        return $this;
    }

    public function removeHabitant(Habitant $habitant): self
    {
        if ($this->habitants->removeElement($habitant)) {
            // set the owning side to null (unless already changed)
            if ($habitant->getDossier() === $this) {
                $habitant->setDossier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures[] = $picture;
            $picture->setDossier($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->pictures->removeElement($picture)) {
            // set the owning side to null (unless already changed)
            if ($picture->getDossier() === $this) {
                $picture->setDossier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getHebergementPictures(): Collection
    {
        return $this->hebergementPictures;
    }

    public function addHebergementPicture(Picture $hebergementPicture): self
    {
        if (!$this->hebergementPictures->contains($hebergementPicture)) {
            $this->hebergementPictures[] = $hebergementPicture;
            $hebergementPicture->setDossierHebergement($this);
        }

        return $this;
    }

    public function removeHebergementPicture(Picture $hebergementPicture): self
    {
        if ($this->hebergementPictures->removeElement($hebergementPicture)) {
            // set the owning side to null (unless already changed)
            if ($hebergementPicture->getDossierHebergement() === $this) {
                $hebergementPicture->setDossierHebergement(null);
            }
        }

        return $this;
    }

    public function getLouerMagasins(): ?string
    {
        return $this->louerMagasins;
    }

    public function setLouerMagasins(?string $louerMagasins): self
    {
        $this->louerMagasins = $louerMagasins;

        return $this;
    }

    public function getMontantLlouerMagasins(): ?string
    {
        return $this->montantLlouerMagasins;
    }

    public function setMontantLlouerMagasins(?string $montantLlouerMagasins): self
    {
        $this->montantLlouerMagasins = $montantLlouerMagasins;

        return $this;
    }

    public function getAvecLaFamille(): ?string
    {
        return $this->avecLaFamille;
    }

    public function setAvecLaFamille(?string $avecLaFamille): self
    {
        $this->avecLaFamille = $avecLaFamille;

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPicturesPereMoert(): Collection
    {
        return $this->picturesPereMoert;
    }

    public function addPicturesPereMoert(Picture $picturesPereMoert): self
    {
        if (!$this->picturesPereMoert->contains($picturesPereMoert)) {
            $this->picturesPereMoert[] = $picturesPereMoert;
            $picturesPereMoert->setDossierPereMort($this);
        }

        return $this;
    }

    public function removePicturesPereMoert(Picture $picturesPereMoert): self
    {
        if ($this->picturesPereMoert->removeElement($picturesPereMoert)) {
            // set the owning side to null (unless already changed)
            if ($picturesPereMoert->getDossierPereMort() === $this) {
                $picturesPereMoert->setDossierPereMort(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPicturesCivil(): Collection
    {
        return $this->picturesCivil;
    }

    public function addPicturesCivil(Picture $picturesCivil): self
    {
        if (!$this->picturesCivil->contains($picturesCivil)) {
            $this->picturesCivil[] = $picturesCivil;
            $picturesCivil->setDossierCivil($this);
        }

        return $this;
    }

    public function removePicturesCivil(Picture $picturesCivil): self
    {
        if ($this->picturesCivil->removeElement($picturesCivil)) {
            // set the owning side to null (unless already changed)
            if ($picturesCivil->getDossierCivil() === $this) {
                $picturesCivil->setDossierCivil(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPicturesMarier(): Collection
    {
        return $this->picturesMarier;
    }

    public function addPicturesMarier(Picture $picturesMarier): self
    {
        if (!$this->picturesMarier->contains($picturesMarier)) {
            $this->picturesMarier[] = $picturesMarier;
            $picturesMarier->setDossierMarier($this);
        }

        return $this;
    }

    public function removePicturesMarier(Picture $picturesMarier): self
    {
        if ($this->picturesMarier->removeElement($picturesMarier)) {
            // set the owning side to null (unless already changed)
            if ($picturesMarier->getDossierMarier() === $this) {
                $picturesMarier->setDossierMarier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPicturesVeuveCin(): Collection
    {
        return $this->picturesVeuveCin;
    }

    public function addPicturesVeuveCin(Picture $picturesVeuveCin): self
    {
        if (!$this->picturesVeuveCin->contains($picturesVeuveCin)) {
            $this->picturesVeuveCin[] = $picturesVeuveCin;
            $picturesVeuveCin->setDossierVeuveCin($this);
        }

        return $this;
    }

    public function removePicturesVeuveCin(Picture $picturesVeuveCin): self
    {
        if ($this->picturesVeuveCin->removeElement($picturesVeuveCin)) {
            // set the owning side to null (unless already changed)
            if ($picturesVeuveCin->getDossierVeuveCin() === $this) {
                $picturesVeuveCin->setDossierVeuveCin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPicturesOrpheCin(): Collection
    {
        return $this->picturesOrpheCin;
    }

    public function addPicturesOrpheCin(Picture $picturesOrpheCin): self
    {
        if (!$this->picturesOrpheCin->contains($picturesOrpheCin)) {
            $this->picturesOrpheCin[] = $picturesOrpheCin;
            $picturesOrpheCin->setDossierOrpheCin($this);
        }

        return $this;
    }

    public function removePicturesOrpheCin(Picture $picturesOrpheCin): self
    {
        if ($this->picturesOrpheCin->removeElement($picturesOrpheCin)) {
            // set the owning side to null (unless already changed)
            if ($picturesOrpheCin->getDossierOrpheCin() === $this) {
                $picturesOrpheCin->setDossierOrpheCin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPicturesRib(): Collection
    {
        return $this->picturesRib;
    }

    public function addPicturesRib(Picture $picturesRib): self
    {
        if (!$this->picturesRib->contains($picturesRib)) {
            $this->picturesRib[] = $picturesRib;
            $picturesRib->setDossierRib($this);
        }

        return $this;
    }

    public function removePicturesRib(Picture $picturesRib): self
    {
        if ($this->picturesRib->removeElement($picturesRib)) {
            // set the owning side to null (unless already changed)
            if ($picturesRib->getDossierRib() === $this) {
                $picturesRib->setDossierRib(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPicturesFamille(): Collection
    {
        return $this->picturesFamille;
    }

    public function addPicturesFamille(Picture $picturesFamille): self
    {
        if (!$this->picturesFamille->contains($picturesFamille)) {
            $this->picturesFamille[] = $picturesFamille;
            $picturesFamille->setDossierFamille($this);
        }

        return $this;
    }

    public function removePicturesFamille(Picture $picturesFamille): self
    {
        if ($this->picturesFamille->removeElement($picturesFamille)) {
            // set the owning side to null (unless already changed)
            if ($picturesFamille->getDossierFamille() === $this) {
                $picturesFamille->setDossierFamille(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPicturesMaison(): Collection
    {
        return $this->picturesMaison;
    }

    public function addPicturesMaison(Picture $picturesMaison): self
    {
        if (!$this->picturesMaison->contains($picturesMaison)) {
            $this->picturesMaison[] = $picturesMaison;
            $picturesMaison->setDossierMaison($this);
        }

        return $this;
    }

    public function removePicturesMaison(Picture $picturesMaison): self
    {
        if ($this->picturesMaison->removeElement($picturesMaison)) {
            // set the owning side to null (unless already changed)
            if ($picturesMaison->getDossierMaison() === $this) {
                $picturesMaison->setDossierMaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPicturesSignature(): Collection
    {
        return $this->picturesSignature;
    }

    public function addPicturesSignature(Picture $picturesSignature): self
    {
        if (!$this->picturesSignature->contains($picturesSignature)) {
            $this->picturesSignature[] = $picturesSignature;
            $picturesSignature->setDossierSinuature($this);
        }

        return $this;
    }

    public function removePicturesSignature(Picture $picturesSignature): self
    {
        if ($this->picturesSignature->removeElement($picturesSignature)) {
            // set the owning side to null (unless already changed)
            if ($picturesSignature->getDossierSinuature() === $this) {
                $picturesSignature->setDossierSinuature(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getMedicalVeuvePictures(): Collection
    {
        return $this->medicalVeuvePictures;
    }

    public function addMedicalVeuvePicture(Picture $medicalVeuvePicture): self
    {
        if (!$this->medicalVeuvePictures->contains($medicalVeuvePicture)) {
            $this->medicalVeuvePictures[] = $medicalVeuvePicture;
            $medicalVeuvePicture->setVeuveMedical($this);
        }

        return $this;
    }

    public function removeMedicalVeuvePicture(Picture $medicalVeuvePicture): self
    {
        if ($this->medicalVeuvePictures->removeElement($medicalVeuvePicture)) {
            // set the owning side to null (unless already changed)
            if ($medicalVeuvePicture->getVeuveMedical() === $this) {
                $medicalVeuvePicture->setVeuveMedical(null);
            }
        }

        return $this;
    }

    public function getSoutienMontant(): ?string
    {
        return $this->soutienMontant;
    }

    public function setSoutienMontant(?string $soutienMontant): self
    {
        $this->soutienMontant = $soutienMontant;

        return $this;
    }

    /**
     * @return Collection|Parrainer[]
     */
    public function getParrainers(): Collection
    {
        return $this->parrainers;
    }

    public function addParrainer(Parrainer $parrainer): self
    {
        if (!$this->parrainers->contains($parrainer)) {
            $this->parrainers[] = $parrainer;
            $parrainer->setDossier($this);
        }

        return $this;
    }

    public function removeParrainer(Parrainer $parrainer): self
    {
        if ($this->parrainers->removeElement($parrainer)) {
            // set the owning side to null (unless already changed)
            if ($parrainer->getDossier() === $this) {
                $parrainer->setDossier(null);
            }
        }

        return $this;
    }

    public function getParrain(): ?Parrains
    {
        return $this->parrain;
    }

    public function setParrain(?Parrains $parrain): self
    {
        $this->parrain = $parrain;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }



    /**
     * @return Collection|ParrainPay[]
     */
    public function getParrainsPay(): Collection
    {
        return $this->parrainsPay;
    }

    public function addParrainsPay(ParrainPay $parrainsPay): self
    {
        if (!$this->parrainsPay->contains($parrainsPay)) {
            $this->parrainsPay[] = $parrainsPay;
            $parrainsPay->setDossier($this);
        }

        return $this;
    }

    public function removeParrainsPay(ParrainPay $parrainsPay): self
    {
        if ($this->parrainsPay->removeElement($parrainsPay)) {
            // set the owning side to null (unless already changed)
            if ($parrainsPay->getDossier() === $this) {
                $parrainsPay->setDossier(null);
            }
        }

        return $this;
    }

    public function getEquipement(): ?array
    {
        return $this->equipement;
    }

    public function setEquipement(?array $equipement): self
    {
        $this->equipement = $equipement;

        return $this;
    }



}
