<?php

namespace App\Entity;

use App\Repository\ParrainPayRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParrainPayRepository::class)
 */
class ParrainPay
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $etat;

    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="parrainsPay")
     * */
    protected $dossier;

    /**
     * @ORM\ManyToOne(targetEntity="Parrains", inversedBy="parrainsPay" )
     * */
    protected $parrain;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(?string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getDossier(): ?Dossier
    {
        return $this->dossier;
    }

    public function setDossier(?Dossier $dossier): self
    {
        $this->dossier = $dossier;

        return $this;
    }

    public function getParrain(): ?Parrains
    {
        return $this->parrain;
    }

    public function setParrain(?Parrains $parrain): self
    {
        $this->parrain = $parrain;

        return $this;
    }

}
