<?php

namespace App\Entity;

use App\Repository\ParrainerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParrainerRepository::class)
 */
class Parrainer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="boolean")
     */
    private $kafala;

    /**
     * @ORM\ManyToOne(targetEntity="Dossier", inversedBy="parrainers")
     * */
    protected $dossier;

    /**
     * @ORM\ManyToOne(targetEntity="SocialAide", inversedBy="parrainers" )
     * */
    protected $socialAide;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDossier(): ?Dossier
    {
        return $this->dossier;
    }

    public function setDossier(?Dossier $dossier): self
    {
        $this->dossier = $dossier;

        return $this;
    }

    public function getSocialAide(): ?SocialAide
    {
        return $this->socialAide;
    }

    public function setSocialAide(?SocialAide $socialAide): self
    {
        $this->socialAide = $socialAide;

        return $this;
    }

    public function getKafala(): ?bool
    {
        return $this->kafala;
    }

    public function setKafala(bool $kafala): self
    {
        $this->kafala = $kafala;

        return $this;
    }


}
