<?php

namespace App\Repository;

use App\Entity\ParrainPay;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ParrainPay|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParrainPay|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParrainPay[]    findAll()
 * @method ParrainPay[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParrainPayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParrainPay::class);
    }

    // /**
    //  * @return ParrainPay[] Returns an array of ParrainPay objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ParrainPay
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
