<?php

namespace App\Repository;

use App\Entity\SocialAide;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SocialAide|null find($id, $lockMode = null, $lockVersion = null)
 * @method SocialAide|null findOneBy(array $criteria, array $orderBy = null)
 * @method SocialAide[]    findAll()
 * @method SocialAide[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SocialAideRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SocialAide::class);
    }

    // /**
    //  * @return SocialAide[] Returns an array of SocialAide objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SocialAide
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
