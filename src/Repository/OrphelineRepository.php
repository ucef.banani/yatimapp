<?php

namespace App\Repository;

use App\Entity\Orpheline;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Orpheline|null find($id, $lockMode = null, $lockVersion = null)
 * @method Orpheline|null findOneBy(array $criteria, array $orderBy = null)
 * @method Orpheline[]    findAll()
 * @method Orpheline[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrphelineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Orpheline::class);
    }

    // /**
    //  * @return Orpheline[] Returns an array of Orpheline objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Orpheline
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
