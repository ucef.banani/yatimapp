<?php

namespace App\Repository;

use App\Entity\Parrains;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Parrains|null find($id, $lockMode = null, $lockVersion = null)
 * @method Parrains|null findOneBy(array $criteria, array $orderBy = null)
 * @method Parrains[]    findAll()
 * @method Parrains[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParrainsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Parrains::class);
    }

    // /**
    //  * @return Parrains[] Returns an array of Parrains objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Parrains
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
