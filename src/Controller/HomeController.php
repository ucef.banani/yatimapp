<?php

namespace App\Controller;

use App\Entity\Dossier;
use App\Entity\Orpheline;
use App\Entity\SocialAide;
use App\Helper\functionHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {

            return $this->redirectToRoute('dash');
    }



    /**
     * @Route("/dash", name="dash")
     */
    public function dash(): Response
    {
        $data = [];


        if( $this->isGranted('ROLE_ADMIN'))
        {
            $data['dossiers'] = $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->createQueryBuilder('a')
                ->select('a')
                ->getQuery()
                ->getResult();

            $data['Totaldossiers'] = $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->createQueryBuilder('a')
                -> select('count(a)')
                // ->andWhere('a.user = :user')
                //->setParameter('user', $this->getUser())
                ->getQuery()
                ->getSingleScalarResult();
        }
        else
        {
            $data['dossiers'] = $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->createQueryBuilder('a')
                ->select('a')
                ->andWhere('a.user = :user')
                ->setParameter('user', $this->getUser())
                ->getQuery()
                ->getResult();

            $data['Totaldossiers'] = $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->createQueryBuilder('a')
                -> select('count(a)')
                ->andWhere('a.user = :user')
                ->setParameter('user', $this->getUser())
                ->getQuery()
                ->getSingleScalarResult();
        }



        $data['socialAides'] = $this->getDoctrine()->getManager()->getRepository(SocialAide::class)->findAll();

        $data['dossierIsValid'] = [];
        $data['TotaldossierIsValid'] = 0;

        $data['TotaldossierNoValid'] = 0;
        $data['dossierNoValid'] = [];

        $data['ScolaireValid']= [];
        $data['totalScolaireValid']= 0;

        $data['ScolaireNoValid']= [];
        $data['totalScolaireNoValid']= 0;



        foreach ($data['dossiers'] as $d) {
            $data['total'] = functionHelper::kafala($d);
            if ($data['total'] < 500) {

                $data['dossierIsValid'] [] = $d;

                $data['TotaldossierIsValid'] = count($data['dossierIsValid']);


                $scolaireValdie = $this->getDoctrine()->getManager()
                    ->getRepository(Orpheline::class)
                    ->createQueryBuilder('a')
                    ->andWhere('a.dossier = :dossier')
                    ->andWhere('a.scolaire != :vide')
                    ->setParameter('vide', "")
                    ->setParameter('dossier', $d)
                    ->getQuery()
                    ->getResult();

                $data['totalScolaireValid'] += count($scolaireValdie);

                foreach ($scolaireValdie as $s) {
                    $data['ScolaireValid'] [] = $s;
                }

            }
            if ($data['total'] > 500) {
                $data['dossierNoValid'] [] = $d;

                $data['TotaldossierNoValid'] = count($data['dossierNoValid']);

                $scolaireNoValdie = $this->getDoctrine()->getManager()
                    ->getRepository(Orpheline::class)
                    ->createQueryBuilder('a')
                    ->andWhere('a.dossier = :dossier')
                    ->andWhere('a.scolaire != :vide')
                    ->setParameter('vide', "")
                    ->setParameter('dossier', $d)
                    ->getQuery()
                    ->getResult();

                $data['totalScolaireNoValid'] += count($scolaireNoValdie);

                foreach ($scolaireNoValdie as $s) {
                    $data['ScolaireNoValid'] [] = $s;
                }
            }

        }
        //dd($data);
        return  $this->render('dash/index.html.twig',$data);

    }
}
