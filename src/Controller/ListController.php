<?php

namespace App\Controller;

use App\Entity\Dossier;
use App\Entity\Orpheline;
use App\Entity\SocialAide;
use App\Helper\functionHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    /**
     * @Route("/list", name="list")
     */
    public function index(): Response
    {
        $data = [];
        $data['user'] = $this->getUser()->getCity();
        $data['socialAides'] = $this->getDoctrine()->getManager()
            ->getRepository(SocialAide::class)
            ->createQueryBuilder('a')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();


         //$data['dossiers'] = $this->getDoctrine()->getManager()->getRepository(Dossier::class)->findAll();

        if( $this->isGranted('ROLE_ADMIN'))
        {
            $data['dossiers'] = $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->createQueryBuilder('a')
                ->getQuery()
                ->getResult();
        }
        else
        {
            $data['dossiers'] = $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->createQueryBuilder('a')
                ->andWhere('a.user = :user')
                ->setParameter('user', $this->getUser())
                ->getQuery()
                ->getResult();
        }


        $data['scolaire'] = array('القسم الخامس', 'القسم التالت');

        if( $this->isGranted('ROLE_ADMIN'))
        {
            $query = $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->createQueryBuilder('a')
                ->getQuery()
                ->getResult();
        }
        else
        {
            $query = $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->createQueryBuilder('a')
                ->andWhere('a.user = :user')
                ->setParameter('user', $this->getUser())
                ->getQuery()
                ->getResult();
        }

        $data['ScolaireValid'] = [];
        $data['totalScolaireValid'] = 0;


        $data['ScolaireValidGirls'] = [];
        $data['totalScolaireValidGirls'] = 0;

        $data['ScolaireValidBoys'] = [];
        $data['totalScolaireValidBoys'] = 0;

        $data['ScolaireValidBoys'] = [];
        $data['totalScolaireValidBoys'] = 0;


        $data['AidValidValid'] = [];
        $data['totalAidValid'] = 0;

        $data['AidValidValidBoys'] = [];
        $data['totalAidValidBoys'] = 0;

        $data['AidValidValidGirls'] = [];
        $data['totalAidValidGirls'] = 0;


        /*no valid*/

        $data['ScolaireNoValid'] = [];
        $data['totalScolaireNoValid'] = 0;


        $data['ScolaireNoValidGirls'] = [];
        $data['totalScolaireNoValidGirls'] = 0;

        $data['ScolaireNoValidBoys'] = [];
        $data['totalScolaireNoValidBoys'] = 0;

        $data['ScolaireNoValidBoys'] = [];
        $data['totalScolaireNoValidBoys'] = 0;


        $data['AidValidNoValid'] = [];
        $data['totalAidNoValid'] = 0;


        foreach ($query as $p) {

            $data['total'] = functionHelper::kafala($p);

            if ($data['total'] < 500) {
                $data['scolaire'] = array('القسم الخامس', 'القسم التالت');

                $data['dossierScolaire'] []  = $p;
                /* الايتام المدرسون*/


                $scolaireValdie = $this->getDoctrine()->getManager()
                    ->getRepository(Orpheline::class)
                    ->createQueryBuilder('a')
                    ->andWhere('a.dossier = :dossier')
                    ->andWhere('a.scolaire != :vide')
                    ->setParameter('vide', "")
                    ->setParameter('dossier', $p)
                    ->getQuery()
                    ->getResult();

                $data['totalScolaireValid'] += count($scolaireValdie);

                foreach ($scolaireValdie as $s) {
                    $data['ScolaireValid'] [] = $s;
                }


                /* الايتام المدرسون انات*/
                $data['girls'] = "أنثى";

                $ScolaireValidGirls = $this->getDoctrine()->getManager()
                    ->getRepository(Orpheline::class)
                    ->createQueryBuilder('a')
                    ->andWhere('a.dossier = :dossier')
                    ->andWhere('a.scolaire != :vide')
                    ->setParameter('vide', "")
                    ->andWhere('a.sexe = (:sexe)')
                    ->setParameter('dossier', $p)
                    //->setParameter('scolaire', $data['scolaire'])
                    ->setParameter('sexe', $data['girls'])
                    ->getQuery()
                    ->getResult();

                $data['totalScolaireValidGirls'] += count($ScolaireValidGirls);

                foreach ($ScolaireValidGirls as $s) {
                    $data['ScolaireValidGirls'] [] = $s;
                }

                /* الايتام المدرسون دكور*/
                $data['boys'] = "ذكر";


                $ScolaireValidBoys = $this->getDoctrine()->getManager()
                    ->getRepository(Orpheline::class)
                    ->createQueryBuilder('a')
                    ->andWhere('a.dossier = :dossier')
                    ->andWhere('a.scolaire != :vide')
                    ->setParameter('vide', "")
                    ->andWhere('a.sexe = (:sexe)')
                    ->setParameter('dossier', $p)
                    //->setParameter('scolaire', $data['scolaire'])
                    ->setParameter('sexe', $data['boys'])
                    ->getQuery()
                    ->getResult();

                $data['totalScolaireValidBoys'] += count($ScolaireValidBoys);

                foreach ($ScolaireValidBoys as $s) {
                    $data['ScolaireValidBoys'] [] = $s;
                }

                /* الاغطية*/

                $data['ValideCouvertures'] [] = $p;



                /*الاضحية*/

                $data['ValideMouton'] [] = $p;

                /* كسوة العيد */

                $AidValidValid = $this->getDoctrine()->getManager()
                    ->getRepository(Orpheline::class)
                    ->createQueryBuilder('a')
                    ->select('a')
                    ->andWhere('a.dossier = :dossier')
                    ->setParameter('dossier', $p)
                    ->getQuery()
                    ->getResult();


                $data['totalAidValid'] += count($AidValidValid);

                foreach ($AidValidValid as $s) {
                    $data['AidValidValid'] [] = $s;
                }

                $AidValidValidBoys = $this->getDoctrine()->getManager()
                    ->getRepository(Orpheline::class)
                    ->createQueryBuilder('a')
                    ->andWhere('a.dossier = :dossier')
                    ->andWhere('a.sexe = (:sexe)')
                    ->setParameter('dossier', $p)
                    //->setParameter('scolaire', $data['scolaire'])
                    ->setParameter('sexe', $data['boys'])
                    ->getQuery()
                    ->getResult();

                $data['totalAidValidBoys'] += count($AidValidValidBoys);

                foreach ($AidValidValidBoys as $s) {
                    $data['AidValidValidBoys'] [] = $s;
                }


                $AidValidValidGirls = $this->getDoctrine()->getManager()
                    ->getRepository(Orpheline::class)
                    ->createQueryBuilder('a')
                    ->andWhere('a.dossier = :dossier')
                    ->andWhere('a.sexe = (:sexe)')
                    ->setParameter('dossier', $p)
                    //->setParameter('scolaire', $data['scolaire'])
                    ->setParameter('sexe', $data['girls'])
                    ->getQuery()
                    ->getResult();

                $data['totalAidValidGirls'] += count($AidValidValidGirls);

                foreach ($AidValidValidBoys as $s) {
                    $data['AidValidValidGirls'] [] = $s;
                }


            }

            if ($data['total'] > 500) {
                $data['scolaire'] = array('القسم الخامس', 'القسم التالت');

                $data['dossierScolaireNoValid'] [] = $p;
                /* الايتام المدرسون*/


                $scolaireValdie = $this->getDoctrine()->getManager()
                    ->getRepository(Orpheline::class)
                    ->createQueryBuilder('a')
                    ->andWhere('a.dossier = :dossier')
                     ->andWhere('a.scolaire != :vide')
                    ->setParameter('vide', "")
                    ->setParameter('dossier', $p)
                    ->getQuery()
                    ->getResult();

                $data['totalScolaireNoValid'] += count($scolaireValdie);

                foreach ($scolaireValdie as $s) {
                    $data['ScolaireNoValid'] [] = $s;
                }


                /* الايتام المدرسون انات*/
                $data['girls'] = "أنثى";

                $ScolaireValidGirls = $this->getDoctrine()->getManager()
                    ->getRepository(Orpheline::class)
                    ->createQueryBuilder('a')
                    ->andWhere('a.dossier = :dossier')
                    ->andWhere('a.scolaire != :vide')
                    ->setParameter('vide', "")
                    ->andWhere('a.sexe = (:sexe)')
                    ->setParameter('dossier', $p)
                   // ->setParameter('scolaire', $data['scolaire'])
                    ->setParameter('sexe', $data['girls'])
                    ->getQuery()
                    ->getResult();

                $data['totalScolaireNoValidGirls'] += count($ScolaireValidGirls);

                foreach ($ScolaireValidGirls as $s) {
                    $data['ScolaireNoValidGirls'] [] = $s;
                }

                /* الايتام المدرسون دكور*/
                $data['boys'] = "ذكر";


                $ScolaireValidBoys = $this->getDoctrine()->getManager()
                    ->getRepository(Orpheline::class)
                    ->createQueryBuilder('a')
                    ->andWhere('a.dossier = :dossier')
                    ->andWhere('a.scolaire != :vide')
                    ->setParameter('vide', "")
                    ->andWhere('a.sexe = (:sexe)')
                    ->setParameter('dossier', $p)
                    //->setParameter('scolaire', $data['scolaire'])
                    ->setParameter('sexe', $data['boys'])
                    ->getQuery()
                    ->getResult();

                $data['totalScolaireNoValidBoys'] += count($ScolaireValidBoys);

                foreach ($ScolaireValidBoys as $s) {
                    $data['ScolaireNoValidBoys'] [] = $s;
                }

                /* الاغطية*/

                $data['NoValideCouvertures'] [] = $p;

                /*الاضحية*/

                $data['NoValideMouton'] [] = $p;

                /* كسوة العيد */

                $AidValidValid = $this->getDoctrine()->getManager()
                    ->getRepository(Orpheline::class)
                    ->createQueryBuilder('a')
                    ->select('a')
                    ->andWhere('a.dossier = :dossier')
                    ->andWhere('a.scolaire IN (:scolaire)')
                    ->setParameter('dossier', $p)
                    ->setParameter('scolaire', $data['scolaire'])
                    ->getQuery()
                    ->getResult();


                $data['totalAidNoValid'] += count($AidValidValid);

                foreach ($AidValidValid as $s) {
                    $data['AidValidNoValid'] [] = $s;
                }

                // dd($data);
            }

        }
       // dd($data);

        $data['dossierIsValid'] = [];
        $data['dossierNoValid'] = [];


        foreach ($query as $d) {
            $data['total'] = functionHelper::kafala($d);
            if ($data['total'] < 500) {
                $data['dossierIsValid'] [] = $d;

            }
            if ($data['total'] > 500) {
                $data['dossierNoValid'] [] = $d;
            }

        }
         //dd($data);
        return $this->render('list/index.html.twig', $data);
    }


    /**
     * @Route("/list/scolaire", name="listScolaire")
     */
    public function listeScolaire(Request $request): Response
    {
        $data = [];

        //$data['dossiers'] = $this->getDoctrine()->getManager()->getRepository(Dossier::class)->findAll();

        if( $this->getUser()->getRoles() == "ROLE_ADMIN")
        {
            $query = $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->createQueryBuilder('a')
                ->getQuery()
                ->getResult();
        }
        else
        {
            $query = $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->createQueryBuilder('a')
                ->andWhere('a.user = :user')
                ->setParameter('user', $this->getUser())
                ->getQuery()
                ->getResult();
        }


        $data['ScolaireValid'] = [];
        $data['totalScolaireValid'] = 0;


        $data['ScolaireNoValid'] = [];
        $data['totalScolaireNoValid'] = 0;

        foreach ($query as $p) {

            $data['total'] = functionHelper::kafala($p);

            if ($data['total'] < 500) {
                //$data['scolaire']=array('القسم الخامس','القسم التالت');

                $data['dossierScolaire'] [] = $p;
                /* الايتام المدرسون*/


                $query = $this->getDoctrine()->getManager()
                    ->getRepository(Orpheline::class)
                    ->createQueryBuilder('a')
                    ->andWhere('a.dossier = :dossier')
                    ->andWhere('a.scolaire != :vide')
                    ->setParameter('vide', "")
                    ->setParameter('dossier', $p);

                if (!empty(trim($request->query->get('scolaire', '')))) {
                    $query->andWhere('a.scolaire =  :scolaire')
                        ->setParameter('scolaire', $request->query->get('scolaire'));
                }

                $data['listeScolaire'] = $query->select('a')
                    ->orderBy('a.id', "DESC")
                    ->getQuery()
                    ->getResult();

                $data['totalScolaireValid'] += count($data['listeScolaire']);

                foreach ($data['listeScolaire'] as $s) {
                    $data['ScolaireValid'] [] = $s;
                }


            }
            if ($data['total'] > 500) {


                $data['dossierNoScolaire'] [] = $p;
                /* الايتام المدرسون*/


                $query = $this->getDoctrine()->getManager()
                    ->getRepository(Orpheline::class)
                    ->createQueryBuilder('a')
                    ->andWhere('a.dossier = :dossier')
                    ->setParameter('dossier', $p);

                if (!empty(trim($request->query->get('scolaire', '')))) {
                    $query->andWhere('a.scolaire =  :scolaire')
                        ->setParameter('scolaire', $request->query->get('scolaire'));
                }

                $data['listeNoScolaire'] = $query->select('a')
                    ->orderBy('a.id', "DESC")
                    ->getQuery()
                    ->getResult();

                $data['totalScolaireNoValid'] += count($data['listeNoScolaire']);

                foreach ($data['listeNoScolaire'] as $s) {
                    $data['ScolaireNoValid'] [] = $s;
                }


            }


            // dd($data);
        }

        // dd($data);
        return $this->render('list/index2.html.twig', $data);
    }
}
