<?php

namespace App\Controller;

use App\Entity\Dossier;
use App\Entity\Orpheline;
use App\Entity\ParrainPay;
use App\Entity\Parrains;
use App\Form\ParrainsType;
use App\Repository\ParrainsRepository;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/parrain")
 */
class ParrainsController extends AbstractController
{
    /**
     * @Route("/", name="parrains_index", methods={"GET"})
     */
    public function index(ParrainsRepository $parrainsRepository): Response
    {
        return $this->render('parrains/index.html.twig', [
            'parrains' => $parrainsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="parrain_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {

        if ($request->isMethod('POST')) {
            $parrain = new Parrains();

            $param = [];
            $param['nom']=$request->request->get('nom','');
            $param['date']=$request->request->get('date','');
            $param['dateDebut']=$request->request->get('dateDebut','');
            $param['dateFin']=$request->request->get('dateFin','');
            $param['mediateur']=$request->request->get('mediateur','');
            $param['tele']=$request->request->get('tele','');
            $param['email']=$request->request->get('email','');
            $param['prix']=$request->request->get('prix','');
            $param['problemePause']=$request->get('problemePause','');


       //    dd($param);

            $parrain->setNom($param['nom']);
            $parrain->setDate(new \DateTime($param['date']));
            $parrain->setDateDebut(new \DateTime($param['dateFin']));
            $parrain->setDateFin(new \DateTime($param['dateFin']));
            $parrain->setMediateur($param['mediateur']);
            $parrain->setTele($param['tele']);
            $parrain->setPrix($param['prix']);
            $parrain->setEmail($param['email']);
            $parrain->setProblemePause($param['problemePause']);


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($parrain);
            $entityManager->flush();



                $param['dossier'] = $this->getDoctrine()
                    ->getManager()
                    ->getRepository(Dossier::class)
                    ->findOneBy(['id' => $request->request->get('dossier_id', '')]);

            if(   $param['dossier'] instanceof Dossier)
            {
                $param['dossier']->setParrain($parrain);
                $entityManager->persist($param['dossier']);
                $entityManager->flush();
                return $this->redirectToRoute('dossier_show',['id'=>$param['dossier']->getId()]);
            }

            return $this->redirectToRoute('parrains_index');
        }
        return $this->render('parrains/new.html.twig');
    }

    /**
     * @Route("/new/dos", name="parrain_new_doss", methods={"GET","POST"})
     */
    public function newIn(Request $request): Response
    {

        if ($request->isMethod('POST')) {

            $param = [];
            $param['dossier'] = $this->getDoctrine()
                ->getManager()
                ->getRepository(Dossier::class)
                ->findOneBy(['id' => $request->request->get('dossier_id', '')]);

            $param['parrain'] = $this->getDoctrine()
                ->getManager()
                ->getRepository(Parrains::class)
                ->findOneBy(['id' => $request->request->get('parrain', '')]);
       // dd($param);
            if(   $param['dossier'] instanceof Dossier && $param['parrain'] instanceof Parrains )
            {
                $entityManager = $this->getDoctrine()->getManager();
                $param['dossier']->setParrain($param['parrain']);
                $entityManager->persist($param['dossier']);
                $entityManager->flush();
                return $this->redirectToRoute('dossier_show',['id'=>$param['dossier']->getId()]);
            }

            return $this->redirectToRoute('parrains_index');
        }
        return $this->render('parrains/new.html.twig');
    }
    /**
     * @Route("/{id}", name="parrains_show", methods={"GET"})
     */
    public function show(Parrains $parrain): Response
    {
        $data=[];
        $data['parrain']=$parrain;
        $data['historique'] = $this->getDoctrine()->getManager()
            ->getRepository(ParrainPay::class)
            ->createQueryBuilder('a')
            ->select('a')
            ->andWhere('a.parrain = :parrain')
            ->setParameter('parrain', $parrain)
            ->addOrderBy('a.id','DESC')
            ->getQuery()
            ->getResult();
        return $this->render('parrains/show.html.twig',$data);
    }

    /**
     * @Route("/{id}/edit", name="parrains_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Parrains $parrain): Response
    {
        if ($request->isMethod('POST')) {

            $param = [];
            $param['nom']=$request->request->get('nom','');
            $param['date']=$request->request->get('date','');
            $param['dateDebut']=$request->request->get('dateDebut','');
            $param['dateFin']=$request->request->get('dateFin','');
            $param['mediateur']=$request->request->get('mediateur','');
            $param['tele']=$request->request->get('tele','');
            $param['email']=$request->request->get('email','');
            $param['prix']=$request->request->get('prix','');
            $param['problemePause']=$request->get('problemePause','');


            //    dd($param);

            $parrain->setNom($param['nom']);
            $parrain->setDate(new \DateTime($param['date']));
            $parrain->setDateDebut(new \DateTime($param['dateFin']));
            $parrain->setDateFin(new \DateTime($param['dateFin']));
            $parrain->setMediateur($param['mediateur']);
            $parrain->setTele($param['tele']);
            $parrain->setPrix($param['prix']);
            $parrain->setEmail($param['email']);
            $parrain->setProblemePause($param['problemePause']);


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($parrain);
            $entityManager->flush();





                $entityManager->flush();
                return $this->redirectToRoute('parrains_show',['id'=>$parrain->getId()]);


        }

        return $this->render('parrains/edit.html.twig', [
            'parrain' => $parrain,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="parrains_delete" )
     */
    public function delete(Request $request, Parrains $parrain , $id): Response
    {
        $parrain = $this->getDoctrine()->getManager()->getRepository(Parrains::class)->find($id);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($parrain);
            $entityManager->flush();


        return $this->redirectToRoute('parrains_index');
    }

    /**
     * @Route("/dossier/deleted/{id}/{{uiid}}", name="parrain_deleted_doss", methods={"GET","POST"})
     */
    public function dossieerDeleted(Request $request , $id , $uiid): Response
    {

            $param = [];
            $param['dossier'] = $this->getDoctrine()
                ->getManager()
                ->getRepository(Dossier::class)->find($id);

            $param['parrain'] = $this->getDoctrine()
                ->getManager()
                ->getRepository(Parrains::class)->find($uiid);
            // dd($param);
            if(   $param['dossier'] instanceof Dossier  )
            {
                $entityManager = $this->getDoctrine()->getManager();
                $param['dossier']->setParrain(Null);
                $entityManager->persist($param['dossier']);
                $entityManager->flush();
                return $this->redirectToRoute('parrains_show',['id'=>$param['parrain']->getId()]);
               // return $this->redirectToRoute('parrains_index');

            }

    }

    /**
     * @Route("/orpheline/deleted/{id}/{{uiid}}", name="parrain_deleted_orpheline", methods={"GET","POST"})
     */
    public function orphelineDeleted(Request $request , $id , $uiid): Response
    {

        $param = [];
        $param['orpheline'] = $this->getDoctrine()
            ->getManager()
            ->getRepository(Orpheline::class)->find($id);

        $param['parrain'] = $this->getDoctrine()
            ->getManager()
            ->getRepository(Parrains::class)->find($uiid);
        // dd($param);
        if(   $param['orpheline'] instanceof Orpheline  )
        {
            $entityManager = $this->getDoctrine()->getManager();
            $param['orpheline']->setParrain(Null);
            $entityManager->persist($param['orpheline']);
            $entityManager->flush();
            return $this->redirectToRoute('parrains_show',['id'=>$param['parrain']->getId()]);
            // return $this->redirectToRoute('parrains_index');

        }



    }




}
