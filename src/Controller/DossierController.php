<?php

namespace App\Controller;

use App\Entity\Dossier;
use App\Entity\Habitant;
use App\Entity\Orpheline;
use App\Entity\Parrainer;
use App\Entity\Parrains;
use App\Entity\Picture;
use App\Entity\SocialAide;
use App\Helper\functionHelper;
use App\Repository\DossierRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * @Route("/dossier")
 */
class DossierController extends AbstractController
{
    /**
     * @Route("/", name="dossier_index", methods={"GET"})
     */
    public function index(DossierRepository $dossierRepository): Response
    {
        $data = [];

       // $data['dossiers'] = $this->getDoctrine()->getManager()->getRepository(Dossier::class)->findAll();

        if( $this->isGranted('ROLE_ADMIN'))
        {
            $data['dossiers'] = $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->createQueryBuilder('a')
                ->getQuery()
                ->getResult();
        }
        else
        {
            $data['dossiers'] = $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->createQueryBuilder('a')
                ->andWhere('a.user = :user')
                ->setParameter('user', $this->getUser())
                ->getQuery()
                ->getResult();
        }


        $data['socialAides'] = $this->getDoctrine()->getManager()
            ->getRepository(SocialAide::class)
            ->createQueryBuilder('a')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();


            /*$this->getDoctrine()->getManager()->getRepository(SocialAide::class)->findAll();*/

        $data['dossierIsValid'] = [];
        $data['dossierNoValid'] = [];


        foreach ($data['dossiers'] as $d) {
            $data['total'] = functionHelper::kafala($d);
            if ($data['total'] < 500) {

                $data['dossierIsValid'] [] = $d;

            }
            if ($data['total'] > 500) {
                $data['dossierNoValid'] [] = $d;
            }

        }
        //dd($data);
        return $this->render('dossier/index.html.twig', $data);
    }


    /**
     * @Route("/index2", name="dossier_index1", methods={"GET"})
     */
    public function index2(): Response
    {
        $data = [];

        $data['dossiers'] = $this->getDoctrine()->getManager()->getRepository(Dossier::class)->findAll();

        $data['dossierIsValid'] = [];
        $data['dossierNoValid'] = [];


        foreach ($data['dossiers'] as $d) {
            $data['total'] = functionHelper::kafala($d);
            if ($data['total'] < 500) {
                $data['dossierIsValid'] [] = $d;
            }
            if ($data['total'] > 500) {
                $data['dossierNoValid'] [] = $d;
            }

        }
        //dd($data);
        //return $this->render('dossier/index.html.twig', $data);
    }

    /**
     * @Route("/new/aideSocial", name="dossier_new_aidSocial", methods={"GET","POST"})
     */
    public function newSocialAide(Request $request): Response
    {
        if ($request->isMethod('POST')) {
            $param = [];
            $parrainer = new Parrainer();
            $param['date'] = $request->request->get('date', '');
            $param['dossier '] = $this->getDoctrine()->getManager()->getRepository(Dossier::class)->find($request->request->get('dossier_id'));
            $param['socialAide '] = $this->getDoctrine()->getManager()->getRepository(SocialAide::class)->find($request->request->get('socialAide_id'));
            if(functionHelper::kafala($param['dossier ']) > 500)
            {
                $parrainer->setKafala(true);
            }
            else{
                $parrainer->setKafala(false);
            }

            $parrainer->setDate(new \DateTime($param['date']));
            $parrainer->setDossier(param['dossier ']);
            $parrainer->setSocialAide($param['socialAide ']);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($parrainer);
            $entityManager->flush();

            return $this->redirectToRoute('dossier_show', ['id' => $param['dossier ']->getId()]);


        }
    }


    /**
     * @Route("/deletee/aideSocial/{id}/{uiid}", name="dossier_delete_aidSocial")
     */
    public function deleteSocialAide(Request $request , $id , $uiid) : Response
    {
        $param['dossier '] = $this->getDoctrine()->getManager()->getRepository(Dossier::class)->find($uiid);
        $param['aideSocial'] = $this->getDoctrine()->getManager()->getRepository(Parrainer::class)->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        //  dd($param);
        $entityManager->remove($param['aideSocial']);
        $entityManager->flush();
        return $this->redirectToRoute('dossier_show', ['id' =>  $param['dossier ']->getId()]);


    }

    /**
     * @Route("/new/aideSocial/ajax", name="dossier_new_aidSocial_ajax" )
     */
    public function AddAideSocialByAjax(Request $request)
    {

        $data = [];
        $data['reponse'] = 'faild';
        $response = new JsonResponse();
        if ($request->isMethod('GET')) {

            $dossier = $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->findOneBy(['id' => $request->query->get('dossier_id', -1)]);
            $social = $this->getDoctrine()->getManager()
                ->getRepository(SocialAide::class)
                ->findOneBy(['id' => $request->query->get('socialAide_id', -1)]);
            //$data['social'] = $social;
            if ($dossier instanceof Dossier && $social instanceof SocialAide) {

                $parrainer = new Parrainer();

                if(functionHelper::kafala($dossier) < 500)
                {
                    $parrainer->setKafala(true);
                }
                else{
                    $parrainer->setKafala(false);
                }

                $parrainer->setDate(new \DateTime());
                $parrainer->setDossier($dossier);
                $parrainer->setSocialAide($social);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($parrainer);
                $entityManager->flush();

                if ($parrainer instanceof Parrainer) {
                    $data['reponse'] = 'success';
                }
            }
        }
        $response->setData($data);
        return $response;
    }

    /**
     * @Route("/new/aideSocial/Dossier", name="dossier_new_aidSocial_liste" , methods={"GET","POST"})
     */
    public function AddAideSocialn(Request $request)
    {

         $data=[];
        if ($request->isMethod('POST')) {

            $dossier = $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->findOneBy(['id' => $request->request->get('dossier_id', -1)]);
            $social = $this->getDoctrine()->getManager()
                ->getRepository(SocialAide::class)
                ->findOneBy(['id' => $request->request->get('socialAide_id', -1)]);


            //dd($data);
            if ($dossier instanceof Dossier && $social instanceof SocialAide) {


                $parrainer = new Parrainer();

                if(functionHelper::kafala($dossier) < 500)
                {
                    $parrainer->setKafala(true);
                }
                else{
                    $parrainer->setKafala(false);
                }


                $parrainer->setDossier($dossier);
                $parrainer->setSocialAide($social);
                $parrainer->setDate((new \DateTime()));

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($parrainer);
                $entityManager->flush();

                if ($parrainer instanceof Parrainer) {
                    return $this->redirectToRoute('dossier_show', ['id' => $dossier->getId()]);
                }
            }
        }

    }


    /**
     * @Route("/new", name="dossier_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        if ($request->isMethod('POST')) {
            $param = [];

            $dossier = new Dossier();


            /* request new */


            $param['date'] = $request->request->get('date', '');
            $param['code'] = $request->request->get('code', '');
            $param['familleNom'] = $request->request->get('familleNom', '');
            $param['region'] = $request->request->get('region', '');
            $param['commune'] = $request->request->get('commune', '');
            $param['addresse'] = $request->request->get('addresse', '');
            $param['tel1'] = $request->request->get('tel1', '');
            $param['tel2'] = $request->request->get('tel2', '');
            $param['tel3'] = $request->request->get('tel3', '');
            $param['nationalite'] = $request->request->get('nationalite', '');

            $param['aideSociale'] = $request->request->get('aideSociale', '');
            $param['montantAideSociale'] = $request->request->get('montantAideSociale', '');
            $param['soutien'] = $request->request->get('soutien', '');
            $param['montantSoutien'] = $request->request->get('montantSoutien', '');


            $param['soutienAssosiation'] = $request->request->get('soutienAssosiation', '');
            $param['montantSoutienAssosiation'] = $request->request->get('montantSoutienAssosiation', '');
            $param['aucunSoutien'] = $request->request->get('aucunSoutien', '');
            $param['autreSoutien'] = $request->request->get('autreSoutien', '');

            /*الممتلكات الموروثة :*/
            $param['louerTerrain'] = $request->request->get('louerTerrain', '');
            $param['montantLouerTerrain'] = $request->request->get('montantLouerTerrain', '');
            $param['louerAppartement'] = $request->request->get('louerAppartement', '');
            $param['montantLouerAppartement'] = $request->request->get('montantLouerAppartement', '');

            $param['louerMagasins'] = $request->request->get('louerMagasins', '');
            $param['montantLlouerMagasins'] = $request->request->get('montantLlouerMagasins', '');

            $param['terreAgricole'] = $request->request->get('terreAgricole', '');
            $param['montantTerreAgricole'] = $request->request->get('montantTerreAgricole', '');
            $param['mouton'] = $request->request->get('mouton', '');
            $param['numberMouton'] = $request->request->get('numberMouton', '');
            $param['aucunProprietes'] = $request->request->get('aucunProprietes', '');


            /*مصاريف السكن*/

            $param['hypotheque'] = $request->request->get('hypotheque', '');
            $param['montantHypotheque'] = $request->request->get('montantHypotheque', '');
            $param['louerHebergement'] = $request->request->get('louerHebergement', '');
            $param['montanyLouerHebergement'] = $request->request->get('montanyLouerHebergement', '');
            $param['AucunHebergement'] = $request->request->get('AucunHebergement', '');

            /*مصاريف الماء والكهرباء*/

            $param['chargeEau'] = $request->request->get('chargeEau', '');
            $param['montantChargeEau'] = $request->request->get('montantChargeEau', '');
            $param['chargeElictricite'] = $request->request->get('chargeElictricite', '');
            $param['montantChargeElictricite'] = $request->request->get('montantChargeElictricite', '');
            $param['aucunchargeEau'] = $request->request->get('aucunchargeEau', '');


            $param['aidez'] = $request->request->get('aidez', '');
            $param['typeHebergement'] = $request->request->get('typeHebergement', '');
            $param['urgentTypeHebergement'] = $request->request->get('urgentTypeHebergement', '');
            $param['adjectif'] = $request->request->get('adjectif', '');
            $param['urgentAdjectif'] = $request->request->get('urgentAdjectif', '');


            /*حالة البناء*/

            $param['construction'] = $request->request->get('aidconstructionez', '');
            $param['urgentConstruction'] = $request->request->get('urgentConstruction', '');
            $param['equipement'] = $request->get('equipement', '0');
            $param['urgentEquipement'] = $request->request->get('urgentEquipement', '');

            /*حالة الاتات*/


            $param['tvBien'] = $request->request->get('tvBien', '');
            $param['tvFible'] = $request->request->get('tvFible', '');
            $param['tvNull'] = $request->request->get('tvNull', '');
            $param['tvBesoin'] = $request->request->get('tvBesoin', '');
            $param['frigoBien'] = $request->request->get('frigoBien', '');
            $param['frigoFible'] = $request->request->get('frigoFible', '');
            $param['frigoNull'] = $request->request->get('frigoNull', '');
            $param['frigoBesoin'] = $request->request->get('frigoBesoin', '');
            $param['couisinBien'] = $request->request->get('couisinBien', '');
            $param['couisinFible'] = $request->request->get('couisinFible', '');
            $param['couisinNull'] = $request->request->get('couisinNull', '');
            $param['couisinBesoin'] = $request->request->get('couisinBesoin', '');
            $param['machineBien'] = $request->request->get('machineBien', '');
            $param['machineFible'] = $request->request->get('machineFible', '');
            $param['machineNull'] = $request->request->get('machineNull', '');
            $param['machineBesoin'] = $request->request->get('machineBesoin', '');
            $param['vetmentBien'] = $request->request->get('vetmentBien', '');
            $param['vetmentFible'] = $request->request->get('vetmentFible', '');
            $param['vetmentNull'] = $request->request->get('vetmentNull', '');
            $param['vetmentBesoin'] = $request->request->get('vetmentBesoin', '');

            $param['matelasBien'] = $request->request->get('matelasBien', '');
            $param['matelasFible'] = $request->request->get('matelasFible', '');
            $param['matelasNull'] = $request->request->get('matelasNull', '');
            $param['matelasBesoin'] = $request->request->get('matelasBesoin', '');
            $param['couverturesBien'] = $request->request->get('couverturesBien', '');
            $param['couverturesFible'] = $request->request->get('couverturesFible', '');
            $param['couverturesNull'] = $request->request->get('couverturesNull', '');
            $param['couverturesBesoin'] = $request->request->get('couverturesBesoin', '');


            /*حالة البناء*/

            $param['materiel'] = $request->request->get('materiel', '');


            $param['pereNom'] = $request->request->get('pereNom', '');
            $param['peretravail'] = $request->request->get('peretravail', '');
            $param['pereDatenissance'] = $request->request->get('pereDatenissance', '');
            $param['pereDatemort'] = $request->request->get('pereDatemort', '');
            $param['pereCin'] = $request->request->get('pereCin', '');
            $param['pereCauseMort'] = $request->request->get('pereCauseMort', '');
            $param['pereEmployeur'] = $request->request->get('pereEmployeur', '');
            $param['pereRetrait'] = $request->request->get('pereRetrait', '');
            $param['pereAccident'] = $request->request->get('pereAccident', '');
            $param['pereRemarque'] = $request->request->get('pereRemarque', '');


            $param['veuve'] = $request->request->get('veuve', '');
            $param['veuveCin'] = $request->request->get('veuveCin', '');

            $param['veuveVetment'] = $request->request->get('veuveVetment', '');
            $param['veuveChaussure'] = $request->request->get('veuveChaussure', '');
            $param['veuveUrgent'] = $request->request->get('veuveUrgent', '');

            $param['orphelinRelation'] = $request->request->get('orphelinRelation', '');
            $param['veuveDateNissance'] = $request->request->get('veuveDateNissance', '');
            $param['veuveScolaire'] = $request->request->get('veuveScolaire', '');
            $param['veuveTraville'] = $request->request->get('veuveTraville', '');
            $param['veuveEmployeur'] = $request->request->get('veuveEmployeur', '');
            $param['veuveSalaire'] = $request->request->get('veuveSalaire', '');
            $param['veuveCharge'] = $request->request->get('veuveCharge', '');
            $param['veuveActivite'] = $request->request->get('veuveActivite', '');
            $param['veuveType'] = $request->request->get('veuveType', '');
            $param['veuveSante'] = $request->request->get('veuveSante', '');
            $param['veuveBank'] = $request->request->get('veuveBank', '');
            $param['veuveRemarque'] = $request->request->get('veuveRemarque', '');


            //  $param['testcheck']=$request->request->get('urgentEquipement','');


            /**Veuve sante*/

            $param['typeMaladie'] = $request->request->get('typeMaladie', '');
            $param['chargeTrv'] = $request->request->get('chargeTrv', '');
            $param['maladieGrave'] = $request->request->get('maladieGrave', '');
            $param['endicape'] = $request->request->get('endicape', '');
            $param['typeEndicape'] = $request->request->get('typeEndicape', '');
            $param['suivie'] = $request->request->get('suivie', '');
            $param['chargeFixe'] = $request->request->get('chargeFixe', '');
            $param['remarque2'] = $request->request->get('remarque2', '');
            $param['casUrgent'] = $request->request->get('casUrgent', '');


            /**almo3il*/

            $param['soutienNom'] = $request->request->get('soutienNom', '');
            $param['avecLaFamille'] = $request->request->get('avecLaFamille', '');
            $param['soutienMontant'] = $request->request->get('soutienMontant', '');
            $param['soutienRelationOrpheline'] = $request->request->get('soutienRelationOrpheline', '');
            $param['soutienTravail'] = $request->request->get('soutienTravail', '');
            $param['soutienType'] = $request->request->get('soutienType', '');


            $dossier->setDate(new \DateTime($param['date']));
            $dossier->setCode($param['code']);
            $dossier->setFamilleNom($param['familleNom']);
            $dossier->setRegion($param['region']);
            $dossier->setCommune($param['commune']);
            $dossier->setAdresse($param['addresse']);
            $dossier->setTel1($param['tel1']);
            $dossier->setTel2($param['tel2']);
            $dossier->setTel3($param['tel3']);
            $dossier->setNationalite($param['nationalite']);


            $dossier->setAideSociale($param['aideSociale']);
            $dossier->setMontantAideSociale($param['montantAideSociale']);
            //$dossier->setProprietes($param['proprietes']);
            $dossier->setSoutien($param['soutien']);
            $dossier->setMontantSoutien($param['montantSoutien']);


            $dossier->setSoutienAssosiation($param['soutienAssosiation']);
            $dossier->setMontantSoutienAssosiation($param['montantSoutienAssosiation']);

            $dossier->setAucunSoutien($param['aucunSoutien']);
            $dossier->setAutreSoutien($param['autreSoutien']);


            $dossier->setLouerTerrain($param['louerTerrain']);
            $dossier->setMontantLouerTerrain($param['montantLouerTerrain']);

            $dossier->setLouerAppartement($param['louerAppartement']);
            $dossier->setMontantLouerAppartement($param['montantLouerAppartement']);

            $dossier->setLouerMagasins($param['louerMagasins']);
            $dossier->setMontantLlouerMagasins($param['montantLlouerMagasins']);

            $dossier->setTerreAgricole($param['terreAgricole']);
            $dossier->setMontantTerreAgricole($param['montantTerreAgricole']);

            $dossier->setMouton($param['mouton']);
            $dossier->setNumberMouton($param['numberMouton']);

            $dossier->setAucunProprietes($param['aucunProprietes']);

            $dossier->setHypotheque($param['hypotheque']);
            $dossier->setMontantHypotheque($param['montantHypotheque']);
            $dossier->setLouerHebergement($param['louerHebergement']);
            $dossier->setMontanyLouerHebergement($param['montanyLouerHebergement']);
            $dossier->setAucunHebergement($param['AucunHebergement']);

            /*مصاريف الماء والكهرباء*/

            $dossier->setChargeEau($param['chargeEau']);
            $dossier->setMontantChargeEau($param['montantChargeEau']);

            $dossier->setChargeElictricite($param['chargeElictricite']);
            $dossier->setMontantChargeElictricite($param['montantChargeElictricite']);

            $dossier->setAucunchargeEau($param['aucunchargeEau']);

            /*المساعدة الخارجية :*/

            $dossier->setAidez($param['aidez']);

            /*وضعية السكن*/


            $dossier->setTypeHebergement($param['typeHebergement']);
            $dossier->setUrgentTypeHebergement($param['urgentTypeHebergement']);

            $dossier->setAdjectif($param['adjectif']);
            $dossier->setUrgentAdjectif($param['urgentAdjectif']);

            $dossier->setConstruction($param['construction']);
            $dossier->setUrgentConstruction($param['urgentConstruction']);
            $dossier->setEquipement($param['equipement']);
            $dossier->setUrgentEquipement($param['urgentEquipement']);
            /*حالة الاتات*/
            /*tv*/
            $dossier->setTvBien($param['tvBesoin']);
            $dossier->setTvFible($param['tvFible']);
            $dossier->setTvNull($param['tvNull']);
            $dossier->setTvBesoin($param['tvBesoin']);

            /*frigo **/

            $dossier->setFrigoBien($param['frigoBien']);
            $dossier->setFrigoFible($param['frigoFible']);
            $dossier->setFrigoNull($param['frigoNull']);
            $dossier->setFrigoBesoin($param['frigoBesoin']);

            /*cousine **/
            $dossier->setCouisinBien($param['couisinBien']);
            $dossier->setCouisinFible($param['couisinFible']);
            $dossier->setCouisinNull($param['couisinNull']);
            $dossier->setCouisinBesoin($param['couisinBesoin']);

            /*cousine **/

            $dossier->setMachineBien($param['couisinBien']);
            $dossier->setMachineFible($param['couisinFible']);
            $dossier->setMachineNull($param['couisinNull']);
            $dossier->setMachineBesoin($param['couisinBesoin']);
            /*vetment **/

            $dossier->setVetmentBien($param['vetmentBien']);
            $dossier->setVetmentFible($param['vetmentFible']);
            $dossier->setVetmentNull($param['vetmentNull']);
            $dossier->setVetmentBesoin($param['vetmentBesoin']);

            /*matlas**/

            $dossier->setMatelasBien($param['matelasBien']);
            $dossier->setMatelasFible($param['matelasFible']);
            $dossier->setMatelasBesoin($param['matelasBesoin']);
            $dossier->setMatelasNull($param['matelasNull']);

            /*couvertures**/

            $dossier->setCouverturesBien($param['couverturesBien']);
            $dossier->setCouverturesFible($param['couverturesFible']);
            $dossier->setCouverturesNull($param['couverturesNull']);
            $dossier->setCouverturesBesoin($param['couverturesBesoin']);

            /*lmo3il*/
            $dossier->setSoutienNom($param['soutienNom']);
            $dossier->setAvecLaFamille($param['avecLaFamille']);
            $dossier->setSoutienMontant($param['soutienMontant']);


            $dossier->setSoutienRelationOrpheline($param['soutienRelationOrpheline']);
            $dossier->setSoutienTravail($param['soutienTravail']);
            $dossier->setSoutienType($param['soutienType']);


            $dossier->setPereNom($param['pereNom']);
            $dossier->setPeretravail($param['peretravail']);
            $dossier->setPereDatenissance(new \DateTime($param['pereDatenissance']));
            $dossier->setPereCin($param['pereCin']);
            $dossier->setPereDatemort(new \DateTime($param['pereDatemort']));
            $dossier->setPereCauseMort($param['pereCauseMort']);
            $dossier->setPereEmployeur($param['pereEmployeur']);
            $dossier->setPereRetrait($param['pereRetrait']);
            $dossier->setPereAccident($param['pereAccident']);
            $dossier->setPereRemarque($param['pereRemarque']);


            $dossier->setVeuve($param['veuve']);
            $dossier->setOrphelinRelation($param['orphelinRelation']);
            $dossier->setVeuveDateNissance(new \DateTime($param['veuveDateNissance']));
            $dossier->setVeuveSalaire($param['veuveSalaire']);
            $dossier->setVeuveCin($param['veuveCin']);


            $dossier->setVeuveVetment($param['veuveVetment']);
            $dossier->setVeuveChaussure($param['veuveChaussure']);
            $dossier->setVeuveUrgent($param['veuveUrgent']);


            $dossier->setVeuveScolaire($param['veuveScolaire']);
            $dossier->setVeuveTraville($param['veuveTraville']);
            $dossier->setVeuveEmployeur($param['veuveEmployeur']);
            $dossier->setVeuveSalaire($param['veuveSalaire']);
            $dossier->setVeuveCharge($param['veuveCharge']);
            $dossier->setVeuveActivite($param['veuveActivite']);
            $dossier->setVeuveType($param['veuveType']);
            $dossier->setVeuveSante($param['veuveSante']);
            $dossier->setVeuveBank($param['veuveBank']);
            $dossier->setVeuveRemarque($param['veuveRemarque']);


            /* sante veuve*/
            $dossier->setTypeMaladie($param['typeMaladie']);
            $dossier->setChargeTrv($param['chargeTrv']);
            $dossier->setMaladieGrave($param['maladieGrave']);
            $dossier->setEndicape($param['endicape']);
            $dossier->setTypeEndicape($param['typeEndicape']);
            $dossier->setSuivie($param['suivie']);
            $dossier->setChargeFixe($param['chargeFixe']);
            $dossier->setRemarque2($param['remarque2']);
            $dossier->setCasUrgent($param['casUrgent']);



            $entityManager = $this->getDoctrine()->getManager();
            $uploadedFile = $request->files->get('medicalVeuvePictures');

            foreach ($uploadedFile as $pic) {
                $file = $pic;
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('uploads_directory'), $fileName);
                $picture = new Picture();
                $picture->setNom($fileName);
                $entityManager->persist($picture);
                $dossier->addMedicalVeuvePicture($picture);

            }


            $entityManager->persist($dossier);
            $entityManager->flush();

            return $this->redirectToRoute('dossier_show', ['id' => $dossier->getId()]);

        }
        return $this->render('dossier/new.html.twig');
    }

    /**
     * @Route("/{id}", name="dossier_show", methods={"GET"})
     */
    public function show(Dossier $dossier, Request $request): Response
    {
        $data = [];
        $data['dossier'] = $dossier;
        $data['dakhl'] = functionHelper::kafala($dossier);
        $data['socialAides'] = $this->getDoctrine()->getManager()->getRepository(SocialAide::class)->findAll();

        $data['parrains'] = $this->getDoctrine()->getManager()->getRepository(Parrains::class)->findAll();
        //dd($data);
        $data['orpheline'] = $this->getDoctrine()->getManager()->getRepository(Orpheline::class)
            ->findOneBy(['dossier' => $dossier, 'id' => $request->query->get('orpheline_id')]);
        $data['habitant'] = $this->getDoctrine()->getManager()->getRepository(Habitant::class)
            ->findOneBy(['dossier' => $dossier, 'id' => $request->query->get('habitant_id')]);
        return $this->render('dossier/show.html.twig', $data);
    }


    /**
     * @Route("/pictures/{id}", name="dossier_show_pictures", methods={"GET"})
     */
    public function showPictures(Dossier $dossier, Request $request): Response
    {
        $data = [];
        $data['dossier'] = $dossier;

        return $this->render('dossier/showPictures.html.twig', $data);
    }


    /**
     * @Route("/addpictures/{id}", name="dossier_add_pictures")
     */
    public function addPictures(Dossier $dossier, Request $request): Response
    {
        $data['dossier'] = $dossier;
        if ($request->isMethod('POST')) {
            $param = [];


            $entityManager = $this->getDoctrine()->getManager();

            $dossierPereMort = $request->files->get('dossierPereMort');
            $dossierCivil = $request->files->get('dossierCivil');
            $dossierMarier = $request->files->get('dossierMarier');
            $dossierVeuveCin = $request->files->get('dossierVeuveCin');
            $dossierRib = $request->files->get('dossierRib');
            $dossierFamille = $request->files->get('dossierFamille');
            $dossierMaison = $request->files->get('dossierMaison');
            $dossierSinuature = $request->files->get('dossierSinuature');

            foreach ($dossierPereMort as $pic) {
                $file = $pic;

                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('uploads_directory'), $fileName);
                $picture = new Picture();
                $picture->setNom($fileName);
                $entityManager->persist($picture);
                $dossier->addPicturesPereMoert($picture);

            }
            foreach ($dossierCivil as $pic) {
                $file = $pic;
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('uploads_directory'), $fileName);
                $picture = new Picture();
                $picture->setNom($fileName);
                $entityManager->persist($picture);
                $dossier->addPicturesCivil($picture);

            }
            foreach ($dossierMarier as $pic) {
                $file = $pic;
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('uploads_directory'), $fileName);
                $picture = new Picture();
                $picture->setNom($fileName);
                $entityManager->persist($picture);
                $dossier->addPicturesMarier($picture);

            }

            foreach ($dossierVeuveCin as $pic) {
                $file = $pic;
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('uploads_directory'), $fileName);
                $picture = new Picture();
                $picture->setNom($fileName);
                $entityManager->persist($picture);
                $dossier->addPicturesVeuveCin($picture);

            }

            foreach ($dossierRib as $pic) {
                $file = $pic;
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('uploads_directory'), $fileName);
                $picture = new Picture();
                $picture->setNom($fileName);
                $entityManager->persist($picture);
                $dossier->addPicturesRib($picture);

            }
            foreach ($dossierFamille as $pic) {
                $file = $pic;
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('uploads_directory'), $fileName);
                $picture = new Picture();
                $picture->setNom($fileName);
                $entityManager->persist($picture);
                $dossier->addPicturesFamille($picture);

            }
            foreach ($dossierMaison as $pic) {
                $file = $pic;
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('uploads_directory'), $fileName);
                $picture = new Picture();
                $picture->setNom($fileName);
                $entityManager->persist($picture);
                $dossier->addPicturesMaison($picture);

            }

            foreach ($dossierSinuature as $pic) {
                $file = $pic;
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('uploads_directory'), $fileName);
                $picture = new Picture();
                $picture->setNom($fileName);
                $entityManager->persist($picture);
                $dossier->addPicturesSignature($picture);

            }

            $entityManager->persist($dossier);
            $entityManager->flush();

            return $this->redirectToRoute('dossier_show_pictures', ['id' => $dossier->getId()]);


        }
        return $this->render('dossier/addPictures.html.twig', $data);
    }


    /**
     * @Route("/{id}/edit", name="dossier_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Dossier $dossier): Response
    {
        if ($request->isMethod('POST')) {
            $param = [];
            $data = [];

            $param['date'] = $request->request->get('date', '');
            $param['code'] = $request->request->get('code', '');
            $param['familleNom'] = $request->request->get('familleNom', '');
            $param['region'] = $request->request->get('region', '');
            $param['commune'] = $request->request->get('commune', '');
            $param['addresse'] = $request->request->get('addresse', '');
            $param['tel1'] = $request->request->get('tel1', '');
            $param['tel2'] = $request->request->get('tel2', '');
            $param['tel3'] = $request->request->get('tel3', '');
            $param['nationalite'] = $request->request->get('nationalite', '');

            $param['aideSociale'] = $request->request->get('aideSociale', '');
            $param['montantAideSociale'] = $request->request->get('montantAideSociale', '');
            $param['soutien'] = $request->request->get('soutien', '');
            $param['montantSoutien'] = $request->request->get('montantSoutien', '');


            $param['soutienAssosiation'] = $request->request->get('soutienAssosiation', '');
            $param['montantSoutienAssosiation'] = $request->request->get('montantSoutienAssosiation', '');
            $param['aucunSoutien'] = $request->request->get('aucunSoutien', '');
            $param['autreSoutien'] = $request->request->get('autreSoutien', '');

            /*الممتلكات الموروثة :*/
            $param['louerTerrain'] = $request->request->get('louerTerrain', '');
            $param['montantLouerTerrain'] = $request->request->get('montantLouerTerrain', '');
            $param['louerAppartement'] = $request->request->get('louerAppartement', '');
            $param['montantLouerAppartement'] = $request->request->get('montantLouerAppartement', '');

            $param['louerMagasins'] = $request->request->get('louerMagasins', '');
            $param['montantLlouerMagasins'] = $request->request->get('montantLlouerMagasins', '');

            $param['terreAgricole'] = $request->request->get('terreAgricole', '');
            $param['montantTerreAgricole'] = $request->request->get('montantTerreAgricole', '');
            $param['mouton'] = $request->request->get('mouton', '');
            $param['numberMouton'] = $request->request->get('numberMouton', '');
            $param['aucunProprietes'] = $request->request->get('aucunProprietes', '');


            /*مصاريف السكن*/

            $param['hypotheque'] = $request->request->get('hypotheque', '');
            $param['montantHypotheque'] = $request->request->get('montantHypotheque', '');
            $param['louerHebergement'] = $request->request->get('louerHebergement', '');
            $param['montanyLouerHebergement'] = $request->request->get('montanyLouerHebergement', '');
            $param['AucunHebergement'] = $request->request->get('AucunHebergement', '');

            /*مصاريف الماء والكهرباء*/

            $param['chargeEau'] = $request->request->get('chargeEau', '');
            $param['montantChargeEau'] = $request->request->get('montantChargeEau', '');
            $param['chargeElictricite'] = $request->request->get('chargeElictricite', '');
            $param['montantChargeElictricite'] = $request->request->get('montantChargeElictricite', '');
            $param['aucunchargeEau'] = $request->request->get('aucunchargeEau', '');


            $param['aidez'] = $request->request->get('aidez', '');
            $param['typeHebergement'] = $request->request->get('typeHebergement', '');
            $param['urgentTypeHebergement'] = $request->request->get('urgentTypeHebergement', '');
            $param['adjectif'] = $request->request->get('adjectif', '');
            $param['urgentAdjectif'] = $request->request->get('urgentAdjectif', '');


            /*حالة البناء*/

            $param['construction'] = $request->request->get('aidconstructionez', '');
            $param['urgentConstruction'] = $request->request->get('urgentConstruction', '');
            $param['equipement'] = $request->get('equipement', '');
            $param['urgentEquipement'] = $request->request->get('urgentEquipement', '');

            /*حالة الاتات*/


            $param['tvBien'] = $request->request->get('tvBien', '');
            $param['tvFible'] = $request->request->get('tvFible', '');
            $param['tvNull'] = $request->request->get('tvNull', '');
            $param['tvBesoin'] = $request->request->get('tvBesoin', '');
            $param['frigoBien'] = $request->request->get('frigoBien', '');
            $param['frigoFible'] = $request->request->get('frigoFible', '');
            $param['frigoNull'] = $request->request->get('frigoNull', '');
            $param['frigoBesoin'] = $request->request->get('frigoBesoin', '');
            $param['couisinBien'] = $request->request->get('couisinBien', '');
            $param['couisinFible'] = $request->request->get('couisinFible', '');
            $param['couisinNull'] = $request->request->get('couisinNull', '');
            $param['couisinBesoin'] = $request->request->get('couisinBesoin', '');
            $param['machineBien'] = $request->request->get('machineBien', '');
            $param['machineFible'] = $request->request->get('machineFible', '');
            $param['machineNull'] = $request->request->get('machineNull', '');
            $param['machineBesoin'] = $request->request->get('machineBesoin', '');
            $param['vetmentBien'] = $request->request->get('vetmentBien', '');
            $param['vetmentFible'] = $request->request->get('vetmentFible', '');
            $param['vetmentNull'] = $request->request->get('vetmentNull', '');
            $param['vetmentBesoin'] = $request->request->get('vetmentBesoin', '');

            $param['matelasBien'] = $request->request->get('matelasBien', '');
            $param['matelasFible'] = $request->request->get('matelasFible', '');
            $param['matelasNull'] = $request->request->get('matelasNull', '');
            $param['matelasBesoin'] = $request->request->get('matelasBesoin', '');
            $param['couverturesBien'] = $request->request->get('couverturesBien', '');
            $param['couverturesFible'] = $request->request->get('couverturesFible', '');
            $param['couverturesNull'] = $request->request->get('couverturesNull', '');
            $param['couverturesBesoin'] = $request->request->get('couverturesBesoin', '');


            /*حالة البناء*/

            $param['materiel'] = $request->request->get('materiel', '');


            $param['pereNom'] = $request->request->get('pereNom', '');
            $param['peretravail'] = $request->request->get('peretravail', '');
            $param['pereDatenissance'] = $request->request->get('pereDatenissance', '');
            $param['pereDatemort'] = $request->request->get('pereDatemort', '');
            $param['pereCin'] = $request->request->get('pereCin', '');
            $param['pereCauseMort'] = $request->request->get('pereCauseMort', '');
            $param['pereEmployeur'] = $request->request->get('pereEmployeur', '');
            $param['pereRetrait'] = $request->request->get('pereRetrait', '');
            $param['pereAccident'] = $request->request->get('pereAccident', '');
            $param['pereRemarque'] = $request->request->get('pereRemarque', '');


            $param['veuve'] = $request->request->get('veuve', '');
            $param['veuveCin'] = $request->request->get('veuveCin', '');

            $param['veuveVetment'] = $request->request->get('veuveVetment', '');
            $param['veuveChaussure'] = $request->request->get('veuveChaussure', '');
            $param['veuveUrgent'] = $request->request->get('veuveUrgent', '');

            $param['orphelinRelation'] = $request->request->get('orphelinRelation', '');
            $param['veuveDateNissance'] = $request->request->get('veuveDateNissance', '');
            $param['veuveScolaire'] = $request->request->get('veuveScolaire', '');
            $param['veuveTraville'] = $request->request->get('veuveTraville', '');
            $param['veuveEmployeur'] = $request->request->get('veuveEmployeur', '');
            $param['veuveSalaire'] = $request->request->get('veuveSalaire', '');
            $param['veuveCharge'] = $request->request->get('veuveCharge', '');
            $param['veuveActivite'] = $request->request->get('veuveActivite', '');
            $param['veuveType'] = $request->request->get('veuveType', '');
            $param['veuveSante'] = $request->request->get('veuveSante', '');
            $param['veuveBank'] = $request->request->get('veuveBank', '');
            $param['veuveRemarque'] = $request->request->get('veuveRemarque', '');


            //  $param['testcheck']=$request->request->get('urgentEquipement','');


            /**Veuve sante*/

            $param['typeMaladie'] = $request->request->get('typeMaladie', '');
            $param['chargeTrv'] = $request->request->get('chargeTrv', '');
            $param['maladieGrave'] = $request->request->get('maladieGrave', '');
            $param['endicape'] = $request->request->get('endicape', '');
            $param['typeEndicape'] = $request->request->get('typeEndicape', '');
            $param['suivie'] = $request->request->get('suivie', '');
            $param['chargeFixe'] = $request->request->get('chargeFixe', '');
            $param['remarque2'] = $request->request->get('remarque2', '');
            $param['casUrgent'] = $request->request->get('casUrgent', '');


            /**almo3il*/

            $param['soutienNom'] = $request->request->get('soutienNom', '');
            $param['avecLaFamille'] = $request->request->get('avecLaFamille', '');
            $param['soutienMontant'] = $request->request->get('soutienMontant', '');
            $param['soutienRelationOrpheline'] = $request->request->get('soutienRelationOrpheline', '');
            $param['soutienTravail'] = $request->request->get('soutienTravail', '');
            $param['soutienType'] = $request->request->get('soutienType', '');


            $dossier->setDate(new \DateTime($param['date']));
            $dossier->setCode($param['code']);
            $dossier->setFamilleNom($param['familleNom']);
            $dossier->setRegion($param['region']);
            $dossier->setCommune($param['commune']);
            $dossier->setAdresse($param['addresse']);
            $dossier->setTel1($param['tel1']);
            $dossier->setTel2($param['tel2']);
            $dossier->setTel3($param['tel3']);
            $dossier->setNationalite($param['nationalite']);


            $dossier->setAideSociale($param['aideSociale']);
            $dossier->setMontantAideSociale($param['montantAideSociale']);
            //$dossier->setProprietes($param['proprietes']);
            $dossier->setSoutien($param['soutien']);
            $dossier->setMontantSoutien($param['montantSoutien']);


            $dossier->setSoutienAssosiation($param['soutienAssosiation']);
            $dossier->setMontantSoutienAssosiation($param['montantSoutienAssosiation']);

            $dossier->setAucunSoutien($param['aucunSoutien']);
            $dossier->setAutreSoutien($param['autreSoutien']);


            $dossier->setLouerTerrain($param['louerTerrain']);
            $dossier->setMontantLouerTerrain($param['montantLouerTerrain']);

            $dossier->setLouerAppartement($param['louerAppartement']);
            $dossier->setMontantLouerAppartement($param['montantLouerAppartement']);

            $dossier->setLouerMagasins($param['louerMagasins']);
            $dossier->setMontantLlouerMagasins($param['montantLlouerMagasins']);

            $dossier->setTerreAgricole($param['terreAgricole']);
            $dossier->setMontantTerreAgricole($param['montantTerreAgricole']);

            $dossier->setMouton($param['mouton']);
            $dossier->setNumberMouton($param['numberMouton']);

            $dossier->setAucunProprietes($param['aucunProprietes']);

            $dossier->setHypotheque($param['hypotheque']);
            $dossier->setMontantHypotheque($param['montantHypotheque']);
            $dossier->setLouerHebergement($param['louerHebergement']);
            $dossier->setMontanyLouerHebergement($param['montanyLouerHebergement']);
            $dossier->setAucunHebergement($param['AucunHebergement']);

            /*مصاريف الماء والكهرباء*/

            $dossier->setChargeEau($param['chargeEau']);
            $dossier->setMontantChargeEau($param['montantChargeEau']);

            $dossier->setChargeElictricite($param['chargeElictricite']);
            $dossier->setMontantChargeElictricite($param['montantChargeElictricite']);

            $dossier->setAucunchargeEau($param['aucunchargeEau']);

            /*المساعدة الخارجية :*/

            $dossier->setAidez($param['aidez']);

            /*وضعية السكن*/


            $dossier->setTypeHebergement($param['typeHebergement']);
            $dossier->setUrgentTypeHebergement($param['urgentTypeHebergement']);

            $dossier->setAdjectif($param['adjectif']);
            $dossier->setUrgentAdjectif($param['urgentAdjectif']);

            $dossier->setConstruction($param['construction']);
            $dossier->setUrgentConstruction($param['urgentConstruction']);
            $dossier->setEquipement($param['equipement']);
            $dossier->setUrgentEquipement($param['urgentEquipement']);
            /*حالة الاتات*/
            /*tv*/
            $dossier->setTvBien($param['tvBesoin']);
            $dossier->setTvFible($param['tvFible']);
            $dossier->setTvNull($param['tvNull']);
            $dossier->setTvBesoin($param['tvBesoin']);

            /*frigo **/

            $dossier->setFrigoBien($param['frigoBien']);
            $dossier->setFrigoFible($param['frigoFible']);
            $dossier->setFrigoNull($param['frigoNull']);
            $dossier->setFrigoBesoin($param['frigoBesoin']);

            /*cousine **/
            $dossier->setCouisinBien($param['couisinBien']);
            $dossier->setCouisinFible($param['couisinFible']);
            $dossier->setCouisinNull($param['couisinNull']);
            $dossier->setCouisinBesoin($param['couisinBesoin']);

            /*cousine **/

            $dossier->setMachineBien($param['couisinBien']);
            $dossier->setMachineFible($param['couisinFible']);
            $dossier->setMachineNull($param['couisinNull']);
            $dossier->setMachineBesoin($param['couisinBesoin']);
            /*vetment **/

            $dossier->setVetmentBien($param['vetmentBien']);
            $dossier->setVetmentFible($param['vetmentFible']);
            $dossier->setVetmentNull($param['vetmentNull']);
            $dossier->setVetmentBesoin($param['vetmentBesoin']);

            /*matlas**/

            $dossier->setMatelasBien($param['matelasBien']);
            $dossier->setMatelasFible($param['matelasFible']);
            $dossier->setMatelasBesoin($param['matelasBesoin']);
            $dossier->setMatelasNull($param['matelasNull']);

            /*couvertures**/

            $dossier->setCouverturesBien($param['couverturesBien']);
            $dossier->setCouverturesFible($param['couverturesFible']);
            $dossier->setCouverturesNull($param['couverturesNull']);
            $dossier->setCouverturesBesoin($param['couverturesBesoin']);

            /*lmo3il*/
            $dossier->setSoutienNom($param['soutienNom']);
            $dossier->setAvecLaFamille($param['avecLaFamille']);
            $dossier->setSoutienMontant($param['soutienMontant']);


            $dossier->setSoutienRelationOrpheline($param['soutienRelationOrpheline']);
            $dossier->setSoutienTravail($param['soutienTravail']);
            $dossier->setSoutienType($param['soutienType']);


            $dossier->setPereNom($param['pereNom']);
            $dossier->setPeretravail($param['peretravail']);
            $dossier->setPereDatenissance(new \DateTime($param['pereDatenissance']));
            $dossier->setPereCin($param['pereCin']);
            $dossier->setPereDatemort(new \DateTime($param['pereDatemort']));
            $dossier->setPereCauseMort($param['pereCauseMort']);
            $dossier->setPereEmployeur($param['pereEmployeur']);
            $dossier->setPereRetrait($param['pereRetrait']);
            $dossier->setPereAccident($param['pereAccident']);
            $dossier->setPereRemarque($param['pereRemarque']);


            $dossier->setVeuve($param['veuve']);
            $dossier->setOrphelinRelation($param['orphelinRelation']);
            $dossier->setVeuveDateNissance(new \DateTime($param['veuveDateNissance']));
            $dossier->setVeuveSalaire($param['veuveSalaire']);
            $dossier->setVeuveCin($param['veuveCin']);


            $dossier->setVeuveVetment($param['veuveVetment']);
            $dossier->setVeuveChaussure($param['veuveChaussure']);
            $dossier->setVeuveUrgent($param['veuveUrgent']);


            $dossier->setVeuveScolaire($param['veuveScolaire']);
            $dossier->setVeuveTraville($param['veuveTraville']);
            $dossier->setVeuveEmployeur($param['veuveEmployeur']);
            $dossier->setVeuveSalaire($param['veuveSalaire']);
            $dossier->setVeuveCharge($param['veuveCharge']);
            $dossier->setVeuveActivite($param['veuveActivite']);
            $dossier->setVeuveType($param['veuveType']);
            $dossier->setVeuveSante($param['veuveSante']);
            $dossier->setVeuveBank($param['veuveBank']);
            $dossier->setVeuveRemarque($param['veuveRemarque']);


            /* sante veuve*/
            $dossier->setTypeMaladie($param['typeMaladie']);
            $dossier->setChargeTrv($param['chargeTrv']);
            $dossier->setMaladieGrave($param['maladieGrave']);
            $dossier->setEndicape($param['endicape']);
            $dossier->setTypeEndicape($param['typeEndicape']);
            $dossier->setSuivie($param['suivie']);
            $dossier->setChargeFixe($param['chargeFixe']);
            $dossier->setRemarque2($param['remarque2']);
            $dossier->setCasUrgent($param['casUrgent']);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($dossier);
            $entityManager->flush();

            return $this->redirectToRoute('dossier_show', ['id' => $dossier->getId()]);
        }
        $data['dossier'] = $dossier;

        return $this->render('dossier/edit.html.twig', $data);
    }

    /**
     * @Route("/delete/{id}", name="dossier_delete" )
     */
    public function delete(Request $request, Dossier $dossier , $id): Response
    {

            $entityManager = $this->getDoctrine()->getManager();
            $dossier =  $entityManager->getRepository(Dossier::class)->find($id);
            $entityManager->remove($dossier);
            $entityManager->flush();


        return $this->redirectToRoute('dossier_index');
    }



    /**
     * @Route("/print/{id}", name="dossier_print", methods={"GET"})
     */
    public function printDossier($id): Response
    {
        $data = array();
        $data['dossier']=$this->getDoctrine()->getManager()->getRepository(Dossier::class)->find($id);
        return $this->render('dossier/pdf.html.twig', $data);



        /*$mpdf = new \Mpdf\Mpdf([
            'margin_left' => 20,
            'margin_right' => 15,
            'margin_top' => 48,
            'margin_bottom' => 25,
            'margin_header' => 10,
            'margin_footer' => 10
        ]);
        // dd($mpdf);


        $html = $this->renderView('dossier/pdf.html.twig', $data);
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("Dossier");

        $mpdf->SetAuthor("X Systems - X BTP");



        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        return new Response($mpdf->Output(), 200, array(
            'Content-Type' => 'application/pdf'
        ));*/


    }

}
