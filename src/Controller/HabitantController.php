<?php

namespace App\Controller;

use App\Entity\Dossier;
use App\Entity\Habitant;
use App\Entity\Picture;
use App\Form\HabitantType;
use App\Repository\HabitantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/habitant")
 */
class HabitantController extends AbstractController
{
    /**
     * @Route("/", name="habitant_index", methods={"GET"})
     */
    public function index(HabitantRepository $habitantRepository): Response
    {
        return $this->render('habitant/index.html.twig', [
            'habitants' => $habitantRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="habitant_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $habitant = new Habitant();

        $param['nom'] = $request->request->get('nom', '');
        $param['relationOrpheline'] = $request->request->get('relationOrpheline', '');
        $param['dateNaissance'] = $request->request->get('dateNaissance', '');
        $param['metier'] = $request->request->get('metier', '');
        $param['etablissement'] = $request->request->get('etablissement', '');
        $param['sallaire'] = $request->request->get('sallaire', '');
        $param['charge'] = $request->request->get('charge', '');
        $param['loisir'] = $request->request->get('loisir', '');
        $param['sante'] = $request->request->get('sante', '');
        $param['remarque'] = $request->request->get('remarque', '');
        $param['typeMaladie'] = $request->request->get('typeMaladie', '');
        $param['chargeTrv'] = $request->request->get('chargeTrv', '');
        $param['maladieGrave'] = $request->request->get('maladieGrave', '');
        $param['endicape'] = $request->request->get('endicape', '');
        $param['typeEndicape'] = $request->request->get('typeEndicape', '');
        $param['suivie'] = $request->request->get('suivie', '');
        $param['chargeFixe'] = $request->request->get('chargeFixe', '');
        $param['remarque2'] = $request->request->get('remarque2', '');
        $param['casUrgent'] = $request->request->get('casUrgent', '');

        $param['dossier'] = $this->getDoctrine()
            ->getManager()
            ->getRepository(Dossier::class)
            ->findOneBy(['id' => $request->request->get('dossier', '')]);


        $habitant->setNom($param['nom']);
        $habitant->setRelationOrpheline($param['relationOrpheline']);
        $habitant->setDateNaissance(new \DateTime($param['dateNaissance']));
        $habitant->setMetier($param['metier']);
        $habitant->setEtablissement($param['etablissement']);

        $habitant->setSallaire($param['sallaire']);
        $habitant->setCharge($param['charge']);
        $habitant->setLoisir($param['loisir']);
        $habitant->setSante($param['sante']);
        $habitant->setRemarque($param['remarque']);
        $habitant->setTypeMaladie($param['typeMaladie']);
        $habitant->setChargeTrv($param['chargeTrv']);
        $habitant->setMaladieGrave($param['maladieGrave']);
        $habitant->setEndicape($param['endicape']);
        $habitant->setTypeEndicape($param['typeEndicape']);
        $habitant->setSuivie($param['suivie']);
        $habitant->setChargeFixe($param['chargeFixe']);
        $habitant->setRemarque2($param['remarque2']);
        $habitant->setCasUrgent($param['casUrgent']);
        $habitant->setDossier($param['dossier']);


        $entityManager = $this->getDoctrine()->getManager();

        $uploadedFile = $request->files->get('habitantPictures');

        foreach ($uploadedFile as  $pic) {
            $file = $pic;
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($this->getParameter('uploads_directory'), $fileName);
            $picture = new Picture();
            $picture->setNom($fileName);
            $entityManager->persist($picture);
            $habitant->addPicture($picture);

        }

        $uploadedMedical = $request->files->get('medicalPictures');

        foreach ($uploadedMedical as  $pic) {
            $file = $pic;
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($this->getParameter('uploads_directory'), $fileName);
            $picture = new Picture();
            $picture->setNom($fileName);
            $entityManager->persist($picture);
            $habitant->addMedicalPicture($picture);

        }

        $entityManager->persist($habitant);
        $entityManager->flush();

        return $this->redirectToRoute('dossier_show',['id'=>$param['dossier']->getId()]);

    }

    /**
     * @Route("/{id}", name="habitant_show", methods={"GET"})
     */
    public function show(Habitant $habitant): Response
    {
        return $this->render('habitant/show.html.twig', [
            'habitant' => $habitant,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="habitant_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Habitant $habitant): Response
    {

    }

    /**
     * @Route("/{id}", name="habitant_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Habitant $habitant): Response
    {
        if ($this->isCsrfTokenValid('delete' . $habitant->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($habitant);
            $entityManager->flush();
        }

        return $this->redirectToRoute('habitant_index');
    }
}
