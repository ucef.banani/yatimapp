<?php

namespace App\Controller;

use App\Entity\Dossier;
use App\Entity\Parrainer;
use App\Entity\SocialAide;
use App\Form\SocialAideType;
use App\Helper\functionHelper;
use App\Repository\SocialAideRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/social/aide")
 */
class SocialAideController extends AbstractController
{
    /**
     * @Route("/", name="social_aide_index", methods={"GET"})
     */
    public function index(SocialAideRepository $socialAideRepository): Response
    {
        $data['social_aides'] = $this->getDoctrine()->getManager()
            ->getRepository(SocialAide::class)
            ->createQueryBuilder('a')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('social_aide/index.html.twig', $data);
    }

    /**
     * @Route("/new", name="social_aide_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        if ($request->isMethod('POST')) {
            $socialAide = new SocialAide();

            $param = [];
            $param['nom'] = $request->request->get('nom', '');
            $param['date'] = $request->request->get('date', '');
            $param['description'] = $request->request->get('description', '');
            $param['categorieVisee'] = $request->request->get('categorieVisee', '');

            $socialAide->setNom($param['nom']);
            $socialAide->setDate(new \DateTime($param['date']));
            $socialAide->setDescription($param['description']);
            $socialAide->setCategorieVisee($param['categorieVisee']);


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($socialAide);
            $entityManager->flush();

            return $this->redirectToRoute('social_aide_show',['id'=>$socialAide->getId()]);

        }

        return $this->render('social_aide/new.html.twig');
    }

    /**
     * @Route("/{id}", name="social_aide_show", methods={"GET"})
     */
    public function show(SocialAide $socialAide, $id): Response
    {


        /* Valid */

        $data['dossiers'] = $this->getDoctrine()->getManager()
            ->getRepository(Parrainer::class)
            ->createQueryBuilder('a')
            ->select('a')
            ->andWhere('a.kafala = true')
           // ->join('a.parrainers', 'p')
            ->join('a.socialAide', 's')
            ->andWhere('s = :socialAide')
            ->setParameter('socialAide', $socialAide)
            ->getQuery()
            ->getResult();

        $data['listDossier'] =[];

        foreach ($data['dossiers'] as $s)
        {
            $data['listDossier'] [$s->getDossier()->getId()] = $s->getDossier();
        }




        $query= $this->getDoctrine()->getManager()
            ->getRepository(Dossier::class)
            ->createQueryBuilder('a')
            ->select('a')
            ->andWhere('a not in (:list)')
            ->setParameter('list', $data['listDossier'])
            ->getQuery()
            ->getResult();

        $data['dossiersNot']= [];

        foreach ($query as $s)
        {
            if(functionHelper::kafala($s) < 500)
            {
                $data['dossiersNot'][]=$s;
            }
        }



        $data['totalDossier'] = count($data['listDossier']);


       $data['totalDossierNot'] = count($data['dossiersNot']);

      // no Valid



        $data['dossiersNoValid'] = $this->getDoctrine()->getManager()
            ->getRepository(Parrainer::class)
            ->createQueryBuilder('a')
            ->select('a')
            ->andWhere('a.kafala = false')
            // ->join('a.parrainers', 'p')
            ->join('a.socialAide', 's')
            ->andWhere('s = :socialAide')
            ->setParameter('socialAide', $socialAide)
            ->getQuery()
            ->getResult();

        $data['listDossierNoValid'] =[];

        foreach ($data['dossiersNoValid'] as $s)
        {
            $data['listDossierNoValid'] [$s->getDossier()->getId()] = $s->getDossier();
        }


        if(count($data['listDossierNoValid']) > 0 )
        {
            $query2= $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->createQueryBuilder('a')
                ->select('a')
                ->andWhere('a not in (:list)')
                 ->setParameter('list', $data['listDossierNoValid'])
                ->getQuery()
                ->getResult();
        }
        else{
            $query2= $this->getDoctrine()->getManager()
                ->getRepository(Dossier::class)
                ->createQueryBuilder('a')
                ->select('a')
                ->getQuery()
                ->getResult();
        }



        $data['dossiersNotValid']= [];

        foreach ($query2 as $s)
        {
            if(functionHelper::kafala($s) > 500)
            {
                $data['dossiersNotValid'][]=$s;
            }
        }

        //dd($data['dossiersNotValid']);
        $data['totalDossierNoValid'] = count($data['listDossierNoValid']);


        $data['totalDossierNotValid'] = count($data['dossiersNotValid']);



        $data['social_aide'] = $this->getDoctrine()->getManager()->getRepository(SocialAide::class)->find($id);

        return $this->render('social_aide/show.html.twig', $data);
    }

    /**
     * @Route("/edit/{id}", name="social_aide_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, $id): Response
    {
       $socialAide = $this->getDoctrine()->getManager()->getRepository(SocialAide::class)->find($id);


            $param = [];
            $param['nom'] = $request->request->get('nom', '');
            $param['date'] = $request->request->get('date', '');
            $param['description'] = $request->request->get('description', '');
            $param['categorieVisee'] = $request->request->get('categorieVisee', '');

            $socialAide->setNom($param['nom']);
            $socialAide->setDate(new \DateTime($param['date']));
            $socialAide->setDescription($param['description']);
            $socialAide->setCategorieVisee($param['categorieVisee']);


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($socialAide);
            $entityManager->flush();

          //  return $this->redirectToRoute('social_aide_index');

        return $this->render('social_aide/edit.html.twig', $socialAide);
    }

    /**
     * @Route("/{id}", name="social_aide_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SocialAide $socialAide): Response
    {
        if ($this->isCsrfTokenValid('delete' . $socialAide->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($socialAide);
            $entityManager->flush();
        }

        return $this->redirectToRoute('social_aide_index');
    }
}
