<?php

namespace App\Controller;

use App\Entity\Dossier;
use App\Entity\Orpheline;
use App\Entity\Parrains;
use App\Entity\Picture;
use App\Form\OrphelineType;
use App\Repository\OrphelineRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/orpheline")
 */
class OrphelineController extends AbstractController
{
    /**
     * @Route("/", name="orpheline_index", methods={"GET"})
     */
    public function index(OrphelineRepository $orphelineRepository): Response
    {
        return $this->render('orpheline/index.html.twig', [
            'orphelines' => $orphelineRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="orpheline_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        if ($request->isMethod('POST')) {


            $orpheline = new Orpheline();

            $param = [];
            $param['nom']=$request->request->get('nom','');
            $param['prenom']=$request->request->get('prenom','');
            $param['sexe']=$request->request->get('sexe','');
            $param['dateNaissance']=$request->request->get('dateNaissance','');
            $param['scolaire']=$request->request->get('scolaire','');
            $param['branche']=$request->request->get('branche','');
            $param['transScolaire']=$request->request->get('transScolaire','');
            $param['transCharge']=$request->request->get('transCharge','');
            $param['autreCharge']=$request->request->get('autreCharge','');
            $param['faiblesseScolaire']=$request->request->get('faiblesseScolaire','');
            $param['pasScolaire']=$request->request->get('pasScolaire','');
            $param['loisir']=$request->request->get('loisir','');
            $param['etablissement']=$request->request->get('etablissement','');
            $param['quoran']=$request->request->get('quoran','');
            $param['pray']=$request->request->get('pray','');
            $param['tele']=$request->request->get('tele','');

            $param['email']=$request->request->get('email','');
            $param['codeMassar']=$request->request->get('codeMassar','');
            $param['passMassar']=$request->request->get('passMassar','');
            $param['typeVetement']=$request->request->get('typeVetement','');
            $param['typeSnate']=$request->request->get('typeSnate','');



            $param['sizeVetment']=$request->request->get('sizeVetment','');
            $param['chaussure']=$request->request->get('chaussure','');
            $param['chausUrgent']=$request->request->get('chausUrgent','');

            $param['remarque']=$request->request->get('remarque','');

            $param['typeMaladie'] = $request->request->get('typeMaladie', '');
            $param['chargeTrv'] = $request->request->get('chargeTrv', '');
            $param['maladieGrave'] = $request->request->get('maladieGrave', '');
            $param['endicape'] = $request->request->get('endicape', '');
            $param['typeEndicape'] = $request->request->get('typeEndicape', '');
            $param['suivie'] = $request->request->get('suivie', '');
            $param['chargeFixe'] = $request->request->get('chargeFixe', '');
            $param['remarque2'] = $request->request->get('remarque2', '');
            $param['casUrgent'] = $request->request->get('casUrgent', '');


            $param['dossier']= $this->getDoctrine()
                ->getManager()
                ->getRepository(Dossier::class)
                ->findOneBy(['id'=>$request->request->get('dossier','')]);

            $orpheline->setNom($param['nom']);
            $orpheline->setPrenom($param['prenom']);
            $orpheline->setSexe($param['sexe']);
            $orpheline->setDateNaissance(new \DateTime($param['dateNaissance']));
            $orpheline->setScolaire($param['scolaire']);
            $orpheline->setBranche($param['branche']);
            $orpheline->setTransCharge($param['transCharge']);
            $orpheline->setTransScolaire($param['transScolaire']);
            $orpheline->setAutreCharge($param['autreCharge']);
            $orpheline->setFaiblesseScolaire($param['faiblesseScolaire']);
            $orpheline->setPasScolaire($param['pasScolaire']);
            $orpheline->setLoisir($param['loisir']);
            $orpheline->setEtablissement($param['etablissement']);
            $orpheline->setQuoran($param['quoran']);
            $orpheline->setPray($param['pray']);
            $orpheline->setTele($param['tele']);

            $orpheline->setEmail($param['email']);
            $orpheline->setCodeMassar($param['codeMassar']);
            $orpheline->setPassMassar($param['passMassar']);

            $orpheline->setTypeSnate($param['typeSnate']);
            $orpheline->setTypeVetement($param['typeVetement']);


            $orpheline->setChaussure($param['chaussure']);
            $orpheline->setChausUrgent($param['chausUrgent']);
            $orpheline->setSizeVetement($param['sizeVetment']);




            $orpheline->setRemarque($param['remarque']);

            $orpheline->setTypeMaladie($param['typeMaladie']);
            $orpheline->setChargeTrv($param['chargeTrv']);
            $orpheline->setMaladieGrave($param['maladieGrave']);
            $orpheline->setEndicape($param['endicape']);
            $orpheline->setTypeEndicape($param['typeEndicape']);
            $orpheline->setSuivie($param['suivie']);
            $orpheline->setChargeFixe($param['chargeFixe']);
            $orpheline->setRemarque2($param['remarque2']);
            $orpheline->setCasUrgent($param['casUrgent']);

            $orpheline->setDossier($param['dossier']);



            $entityManager = $this->getDoctrine()->getManager();

            $uploadedFile = $request->files->get('picturesOrph');



            foreach ($uploadedFile as  $pic) {
                $file = $pic;
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('uploads_directory'), $fileName);
                $picture = new Picture();
                $picture->setNom($fileName);

                $entityManager->persist($picture);
               // dd($picture);
                $orpheline->addPicture($picture);

            }

            $uploadedMedical = $request->files->get('medicalPictures');

            foreach ($uploadedMedical as  $pic) {
                $file = $pic;
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('uploads_directory'), $fileName);
                $picture = new Picture();
                $picture->setNom($fileName);
                $entityManager->persist($picture);
                $orpheline->addMedicalPicture($picture);

            }



            $entityManager->persist($orpheline);
            $entityManager->flush();

            return $this->redirectToRoute('dossier_show',['id'=>$param['dossier']->getId()]);
            //dd($orpheline);


        }

    }

    /**
     * @Route("/{id}", name="orpheline_show", methods={"GET"})
     */
    public function show(Orpheline $orpheline): Response
    {
        return $this->render('orpheline/show.html.twig', [
            'orpheline' => $orpheline,
        ]);
    }

    /**
     * @Route("edit/{id}/", name="orpheline_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Orpheline $orpheline): Response
    {

        if($request->isMethod('POST')){

            $param = [];
            $param['nom']=$request->request->get('nom','');
            $param['prenom']=$request->request->get('prenom','');
            $param['sexe']=$request->request->get('sexe','');
            $param['dateNaissance']=$request->request->get('dateNaissance','');
            $param['scolaire']=$request->request->get('scolaire','');
            $param['branche']=$request->request->get('branche','');
            $param['transScolaire']=$request->request->get('transScolaire','');
            $param['transCharge']=$request->request->get('transCharge','');
            $param['autreCharge']=$request->request->get('autreCharge','');
            $param['faiblesseScolaire']=$request->request->get('faiblesseScolaire','');
            $param['pasScolaire']=$request->request->get('pasScolaire','');
            $param['loisir']=$request->request->get('loisir','');
            $param['etablissement']=$request->request->get('etablissement','');
            $param['etablissement']=$request->request->get('etablissement','');
            $param['quoran']=$request->request->get('quoran','');
            $param['pray']=$request->request->get('pray','');
            $param['tele']=$request->request->get('tele','');
            $param['email']=$request->request->get('email','');
            $param['codeMassar']=$request->request->get('codeMassar','');
            $param['passMassar']=$request->request->get('passMassar','');
            $param['typeVetement']=$request->request->get('typeVetement','');

            $param['chaussure']=$request->request->get('chaussure','');
            $param['chausUrgent']=$request->request->get('chausUrgent','');

            $param['typeSnate']=$request->request->get('typeSnate','');
            $param['remarque']=$request->request->get('remarque','');

            $param['typeMaladie'] = $request->request->get('typeMaladie', '');
            $param['chargeTrv'] = $request->request->get('chargeTrv', '');
            $param['maladieGrave'] = $request->request->get('maladieGrave', '');
            $param['endicape'] = $request->request->get('endicape', '');
            $param['typeEndicape'] = $request->request->get('typeEndicape', '');
            $param['suivie'] = $request->request->get('suivie', '');
            $param['chargeFixe'] = $request->request->get('chargeFixe', '');
            $param['remarque2'] = $request->request->get('remarque2', '');
            $param['casUrgent'] = $request->request->get('casUrgent', '');


            $param['dossier']= $this->getDoctrine()
                ->getManager()
                ->getRepository(Dossier::class)
                ->findOneBy(['id'=>$request->request->get('dossier','')]);

            $orpheline->setNom($param['nom'], $orpheline->getNom());
            $orpheline->setPrenom($param['prenom'] , $orpheline->getPrenom());
            $orpheline->setSexe($param['sexe'],$orpheline->getSexe());
            $orpheline->setDateNaissance(new \DateTime($param['dateNaissance']), $orpheline->setDateNaissance(new \DateTime()));
            $orpheline->setScolaire($param['scolaire'], $orpheline->getScolaire());
            $orpheline->setBranche($param['branche'], $orpheline->getBranche());
            $orpheline->setTransCharge($param['transCharge'], $orpheline->getTransCharge());
            $orpheline->setTransScolaire($param['transScolaire'], $orpheline->getTransScolaire());
            $orpheline->setAutreCharge($param['autreCharge'], $orpheline->getAutreCharge());
            $orpheline->setFaiblesseScolaire($param['faiblesseScolaire'], $orpheline->getFaiblesseScolaire());
            $orpheline->setPasScolaire($param['pasScolaire'], $orpheline->getPasScolaire());
            $orpheline->setLoisir($param['loisir'], $orpheline->getLoisir());
            $orpheline->setEtablissement($param['etablissement'], $orpheline->getEtablissement());
            $orpheline->setQuoran($param['quoran'], $orpheline->getQuoran());
            $orpheline->setPray($param['pray'], $orpheline->getPray());
            $orpheline->setTele($param['tele'], $orpheline->getTele());
            $orpheline->setTypeSnate($param['typeSnate'], $orpheline->getTypeSnate());
            $orpheline->setTypeVetement($param['typeVetement'], $orpheline->getTypeVetement());


            $orpheline->setChaussure($param['chaussure'], $orpheline->getChaussure());
            $orpheline->setChausUrgent($param['chausUrgent'], $orpheline->getChausUrgent());

            $orpheline->setRemarque($param['remarque'], $orpheline->getRemarque());

            $orpheline->setTypeMaladie($param['typeMaladie'], $orpheline->getTypeMaladie());
            $orpheline->setChargeTrv($param['chargeTrv'], $orpheline->getChargeTrv());
            $orpheline->setMaladieGrave($param['maladieGrave'], $orpheline->getMaladieGrave());
            $orpheline->setEndicape($param['endicape'], $orpheline->getEndicape());
            $orpheline->setTypeEndicape($param['typeEndicape'], $orpheline->getTypeEndicape());
            $orpheline->setSuivie($param['suivie'], $orpheline->getSuivie());
            $orpheline->setChargeFixe($param['chargeFixe'], $orpheline->getChargeFixe());
            $orpheline->setRemarque2($param['remarque2'], $orpheline->getRemarque2());
            $orpheline->setCasUrgent($param['casUrgent'], $orpheline->getCasUrgent());

            $orpheline->setDossier($param['dossier']);



            $entityManager = $this->getDoctrine()->getManager();

             $uploadedFile = $request->files->get('picturesOrph');

            foreach ($uploadedFile as  $pic) {
                $file = $pic;
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('uploads_directory'), $fileName);
                $picture = new Picture();
                $picture->setNom($fileName);
                $entityManager->persist($picture);
                $orpheline->addPicture($picture);

            }

            $uploadedMedical = $request->files->get('medicalPictures');

            foreach ($uploadedMedical as  $pic) {
                $file = $pic;
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('uploads_directory'), $fileName);
                $picture = new Picture();
                $picture->setNom($fileName);
                $entityManager->persist($picture);
                $orpheline->addMedicalPicture($picture);

            }

            $entityManager->persist($orpheline);
            $entityManager->flush();

            return $this->redirectToRoute('dossier_show',['id'=>$param['dossier']->getId()]);

        }

    }

    /**
     * @Route("/new/dos", name="parrain_new_orpheline", methods={"GET","POST"})
     */
    public function newParr(Request $request): Response
    {
        if ($request->isMethod('POST')) {

            $param = [];
            $param['orpheline'] = $this->getDoctrine()
                ->getManager()
                ->getRepository(Orpheline::class)
                ->findOneBy(['id' => $request->request->get('orpheline_id', '')]);

            $param['parrain'] = $this->getDoctrine()
                ->getManager()
                ->getRepository(Parrains::class)
                ->findOneBy(['id' => $request->request->get('parrain', '')]);

            if(   $param['orpheline'] instanceof Orpheline && $param['parrain'] instanceof Parrains )
            {
                $entityManager = $this->getDoctrine()->getManager();
                $param['orpheline']->setParrain($param['parrain']);
                $entityManager->persist($param['orpheline']);
                $entityManager->flush();
                return $this->redirectToRoute('dossier_show',['id'=>$param['orpheline']->getDossier()->getId()]);
            }

            return $this->redirectToRoute('dash');
        }

    }


    /**
     * @Route("/{id}", name="orpheline_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Orpheline $orpheline): Response
    {
        if ($this->isCsrfTokenValid('delete' . $orpheline->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($orpheline);
            $entityManager->flush();
        }

        return $this->redirectToRoute('orpheline_index');
    }
}
