<?php

namespace App\Controller;

use App\Entity\Dossier;
use App\Entity\ParrainPay;
use App\Entity\Parrains;
use App\Form\ParrainPayType;
use App\Repository\ParrainPayRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/parrain/pay")
 */
class ParrainPayController extends AbstractController
{
    /**
     * @Route("/", name="parrain_pay_index", methods={"GET"})
     */
    public function index(ParrainPayRepository $parrainPayRepository): Response
    {
        return $this->render('parrain_pay/index.html.twig', [
            'parrain_pays' => $parrainPayRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="parrain_pay_new", methods={"GET","POST"})
     */
    public function new(Request $request)
    {
        $data=[];
        $data['reponse'] = 'faild';
        $response = new JsonResponse();
        if ($request->isMethod('GET')) {

            $data['dossier']= $this->getDoctrine()
                ->getManager()
                ->getRepository(Dossier::class)
                ->findOneBy(
                    ['id' => $request->get('dossier_id', '')]
                );
            $data['parrain']= $this->getDoctrine()
                ->getManager()
                ->getRepository(Parrains::class)
                ->findOneBy(
                    ['id' => $request->get('parrain_id', '')]
                );


            if($data['dossier'] instanceof Dossier && $data['parrain'] instanceof Parrains)
            {
                $parrainPay = new ParrainPay();
                $entityManager = $this->getDoctrine()->getManager();

                $parrainPay->setDate(new \DateTime());
                $parrainPay->setEtat('1');
                $parrainPay->setDossier($data['dossier']);
                $parrainPay->setParrain($data['parrain']);

                $entityManager->persist($parrainPay);
                $entityManager->flush();

                if ($parrainPay instanceof ParrainPay) {
                    $data['reponse'] = 'success';
                }
            }


        }
        $response->setData($data);
        return $response;


    }



    /**
     * @Route("/new/all/{id}", name="parrain_pay_new_all", methods={"GET","POST"})
     */
    public function newAll(Request $request , $id): Response
    {


            $data['parrain']= $this->getDoctrine()
                ->getManager()
                ->getRepository(Parrains::class)
                ->find($id);

            foreach ($data['parrain']->getDossier() as $dossier )
            {
                $parrainPay = new ParrainPay();
                $entityManager = $this->getDoctrine()->getManager();

                $parrainPay->setDate(new \DateTime());
                $parrainPay->setEtat('1');
                $parrainPay->setDossier($dossier);
                $parrainPay->setParrain($data['parrain']);

                $entityManager->persist($parrainPay);
                $entityManager->flush();

            }
            dd($data);


    }

    /**
     * @Route("/{id}", name="parrain_pay_show", methods={"GET"})
     */
    public function show(ParrainPay $parrainPay): Response
    {
        return $this->render('parrain_pay/show.html.twig', [
            'parrain_pay' => $parrainPay,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="parrain_pay_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ParrainPay $parrainPay): Response
    {
        $form = $this->createForm(ParrainPayType::class, $parrainPay);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('parrain_pay_index');
        }

        return $this->render('parrain_pay/edit.html.twig', [
            'parrain_pay' => $parrainPay,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="parrain_pay_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ParrainPay $parrainPay): Response
    {
        if ($this->isCsrfTokenValid('delete'.$parrainPay->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($parrainPay);
            $entityManager->flush();
        }

        return $this->redirectToRoute('parrain_pay_index');
    }
}
