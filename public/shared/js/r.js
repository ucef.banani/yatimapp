//responsive picture
$(function () {
    $('[data-bg-src]').each(function () {
        var src= $(this).attr('data-bg-src');
        $(this).css('background-image','url("'+src+'")');
        $(this).css('background-size','cover');
        $(this).css('background-position','center');
    });
});